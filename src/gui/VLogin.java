/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package gui;

import dao.DaoUtilizadores;

import driver.ConnDb;

import model.MUtilizadores;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


// TODO: Auto-generated Javadoc
/**
 * The Class VLogin.
 */
@SuppressWarnings("serial")
public class VLogin extends JFrame {
    
    /** The tipo utili. */
    private static int tipoUtili;

    /** The modelo. */
    MUtilizadores modelo = new MUtilizadores();

    /** The control. */
    DaoUtilizadores control = new DaoUtilizadores();

    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The content pane. */
    private JPanel contentPane;

    /** The nome. */
    private JTextField nome;

    /** The password. */
    private JPasswordField password;

    /**
     * Create the frame.
     */
    public VLogin() {
        setResizable(false);
        setTitle("Autenticação");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 399, 249);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon(VLogin.class.getResource(
                    "/imagens/Devices-secure-card-icon.png")));
        lblNewLabel.setBounds(10, 11, 134, 115);
        contentPane.add(lblNewLabel);

        JLabel lblNewLabel1 = new JLabel("Nome de Utilizador :");
        lblNewLabel1.setBounds(147, 46, 108, 14);
        contentPane.add(lblNewLabel1);

        JLabel lblNewLabel2 = new JLabel("Password :");
        lblNewLabel2.setBounds(147, 89, 108, 14);
        contentPane.add(lblNewLabel2);

        nome = new JTextField();
        nome.setBounds(265, 43, 108, 20);
        contentPane.add(nome);
        nome.setColumns(10);

        password = new JPasswordField();
        password.setBounds(265, 86, 108, 20);
        contentPane.add(password);

        JButton btnNewButton = new JButton("");
        btnNewButton.addActionListener(new ActionListener() {
                @SuppressWarnings("deprecation")
                public void actionPerformed(ActionEvent action) {
                    modelo.setNome(nome.getText());
                    modelo.setPassword(password.getText());

                    if (control.loginDb(modelo) == true) {
                        tipoUtili = modelo.getTipo();

                        VCarlog janela = new VCarlog(tipoUtili);

                        janela.setVisible(true);

                        dispose();
                    } else {
                        nome.setText("");
                        password.setText("");
                    }
                }
            });
        btnNewButton.setIcon(new ImageIcon(VLogin.class.getResource(
                    "/imagens/Ok-icon.png")));
        btnNewButton.setBounds(147, 144, 81, 52);
        contentPane.add(btnNewButton);

        JButton btnNewButton1 = new JButton("");
        btnNewButton1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    System.exit(0);
                }
            });
        btnNewButton1.setIcon(new ImageIcon(VLogin.class.getResource(
                    "/imagens/Button-Turn-Off-icon.png")));
        btnNewButton1.setBounds(292, 144, 81, 52);
        contentPane.add(btnNewButton1);
    }

    /**
     * Launch the application.
     *
     * @param args
     *          the arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
                /*
                 * (non-Javadoc)
                 *
                 * @see java.lang.Runnable#run()
                 */
                public void run() {
                    try {
                        VLogin frame = new VLogin();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir janela Login! \n" + erro);
                    }
                }
            });
    }
}
