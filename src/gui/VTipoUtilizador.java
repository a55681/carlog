/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package gui;

import dao.DaoTipoUtilizador;

import driver.ConnDb;

import model.MTipoUtilizadores;

import net.proteanit.sql.DbUtils;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;


// TODO: Auto-generated Javadoc
/**
 * The Class VTipoUtilizador.
 */
@SuppressWarnings("serial")
public class VTipoUtilizador extends JFrame {
    
    /** The modelo. */
    MTipoUtilizadores modelo = new MTipoUtilizadores();

    /** The control. */
    DaoTipoUtilizador control = new DaoTipoUtilizador();

    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The flag. */
    int flag = 0;

    /** The content pane. */
    private JPanel contentPane;

    /** The descricao. */
    private JTextField descricao;

    /** The tableatipoutilizador. */
    private JTable tableatipoutilizador;

    /** The pesquisar. */
    private JTextField pesquisar;

    /** The tipo utilizadores id. */
    private JTextField tipoUtilizadoresId;

    /**
     * Instantiates a new v automovel.
     *
     * @throws ParseException
     *           the parse exception
     */
    public VTipoUtilizador() throws ParseException {
        setResizable(false);
        setBackground(Color.WHITE);
        setTitle("Tipo Utilizador");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 723, 242);
        contentPane = new JPanel();
        contentPane.setBackground(Color.WHITE);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("Descrição :");
        lblNewLabel.setBounds(159, 116, 53, 14);
        contentPane.add(lblNewLabel);
        descricao = new JTextField();
        descricao.setEnabled(false);
        descricao.setBounds(222, 113, 121, 20);
        contentPane.add(descricao);
        descricao.setColumns(10);

        JLabel lblNewLabel3 = new JLabel("");
        lblNewLabel3.setIcon(new ImageIcon(VTipoUtilizador.class.getResource(
                    "/imagens/1466017411_user.png")));
        lblNewLabel3.setBounds(10, 13, 139, 138);
        contentPane.add(lblNewLabel3);

        JButton btnNewButton1 = new JButton("");
        btnNewButton1.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    dispose();
                }
            });

        btnNewButton1.setIcon(new ImageIcon(VTipoUtilizador.class.getResource(
                    "/imagens/Button-Close-icon.png")));
        btnNewButton1.setBounds(631, 162, 78, 36);
        contentPane.add(btnNewButton1);

        final JButton btnadicionar = new JButton("Adicionar");
        btnadicionar.setIcon(new ImageIcon(VTipoUtilizador.class.getResource(
                    "/imagens/Button-Add-icon.png")));
        btnadicionar.setBounds(11, 162, 113, 36);
        contentPane.add(btnadicionar);

        final JButton btnguardar = new JButton("Guardar");
        btnguardar.setEnabled(false);
        btnguardar.setIcon(new ImageIcon(VTipoUtilizador.class.getResource(
                    "/imagens/1466018858_floppy_disk_save.png")));
        btnguardar.setBounds(135, 162, 113, 36);
        contentPane.add(btnguardar);

        final JButton btncancelar = new JButton("Cancelar");
        btncancelar.setEnabled(false);
        btncancelar.setIcon(new ImageIcon(VTipoUtilizador.class.getResource(
                    "/imagens/Button-Cancel-icon.png")));
        btncancelar.setBounds(259, 162, 113, 36);
        contentPane.add(btncancelar);

        final JButton btneditar = new JButton("Editar");
        btneditar.setEnabled(false);
        btneditar.setIcon(new ImageIcon(VTipoUtilizador.class.getResource(
                    "/imagens/Text-Edit-icon.png")));
        btneditar.setBounds(383, 162, 113, 36);
        contentPane.add(btneditar);

        final JButton btnapagar = new JButton("Apagar");
        btnapagar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    int resposta = 0;
                    resposta = JOptionPane.showConfirmDialog(rootPane,
                            "Tem a Certeza que quer apagar?");

                    if (resposta == JOptionPane.YES_OPTION) {
                        modelo.setTipoUtilizadoresId(Integer.parseInt(
                                tipoUtilizadoresId.getText()));
                        control.apagarDb(modelo);
                        tipoUtilizadoresId.setText("");
                        descricao.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnapagar.setEnabled(false);
                        btneditar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        tipoUtilizadoresId.setEnabled(false);
                        descricao.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela("select * from tipoutilizador");
                    }
                }
            });

        btnapagar.setEnabled(false);
        btnapagar.setIcon(new ImageIcon(VTipoUtilizador.class.getResource(
                    "/imagens/Button-Delete-icon.png")));
        btnapagar.setBounds(507, 162, 113, 36);
        contentPane.add(btnapagar);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(411, 61, 298, 89);
        contentPane.add(scrollPane);
        tableatipoutilizador = new JTable();
        tableatipoutilizador.setFocusTraversalKeysEnabled(false);
        scrollPane.setViewportView(tableatipoutilizador);
        tableatipoutilizador.setBorder(UIManager.getBorder(
                "Table.scrollPaneBorder"));
        pesquisar = new JTextField();
        pesquisar.setBounds(411, 27, 129, 20);
        contentPane.add(pesquisar);
        pesquisar.setColumns(10);

        JButton btnpesquisar = new JButton("Procurar");
        btnpesquisar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    modelo.setPesquisa(pesquisar.getText());

                    MTipoUtilizadores modelorecebe = control.procuratipo(modelo);
                    tipoUtilizadoresId.setText(String.valueOf(
                            modelorecebe.getTipoUtilizadoresId()));
                    descricao.setText(modelorecebe.getDescricao());
                    btnadicionar.setEnabled(true);
                    btnguardar.setEnabled(false);
                    btncancelar.setEnabled(false);
                    btneditar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    preencheTabela(
                        "select * from tipoutilizador where descricao like'%" +
                        modelo.getPesquisa() + "%'");
                }
            });

        btnpesquisar.setIcon(new ImageIcon(VTipoUtilizador.class.getResource(
                    "/imagens/search-button-icon.png")));
        btnpesquisar.setBounds(564, 26, 145, 23);
        contentPane.add(btnpesquisar);

        JLabel id1 = new JLabel("ID :");
        id1.setBounds(159, 71, 70, 14);
        contentPane.add(id1);
        tipoUtilizadoresId = new JTextField();
        tipoUtilizadoresId.setEnabled(false);
        tipoUtilizadoresId.setBounds(241, 68, 70, 20);
        contentPane.add(tipoUtilizadoresId);
        tipoUtilizadoresId.setColumns(10);

        btnadicionar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    flag = 1;
                    btnguardar.setEnabled(true);
                    descricao.setEnabled(true);
                    btncancelar.setEnabled(true);
                    preencheTabela("select * from tipoutilizador");
                }
            });

        btneditar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    flag = 2;
                    btnguardar.setEnabled(true);
                    btncancelar.setEnabled(true);
                    btneditar.setEnabled(false);
                    btnadicionar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    descricao.setEnabled(true);
                    pesquisar.setEnabled(true);
                    preencheTabela("select * from tipoutilizador");
                }
            });
        btncancelar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    btnguardar.setEnabled(!true);
                    btncancelar.setEnabled(!true);
                    btnadicionar.setEnabled(true);
                    btneditar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    tipoUtilizadoresId.setText("");
                    descricao.setText("");
                    pesquisar.setText("");
                    preencheTabela("select * from tipoutilizador");
                }
            });

        btnguardar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent arg0) {
                    if (flag == 1) {
                        modelo.setDescricao(descricao.getText());
                        control.gravarDb(modelo);
                        tipoUtilizadoresId.setText("");
                        descricao.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        tipoUtilizadoresId.setEnabled(false);
                        descricao.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela("select * from tipoutilizador");
                    } else {
                        modelo.setTipoUtilizadoresId(Integer.parseInt(
                                tipoUtilizadoresId.getText()));
                        modelo.setDescricao(descricao.getText());
                        control.editarDb(modelo);
                        tipoUtilizadoresId.setText("");
                        descricao.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        tipoUtilizadoresId.setEnabled(false);
                        descricao.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela("select * from tipoutilizador");
                    }
                }
            });

        tableatipoutilizador.addMouseListener(new MouseAdapter() {
                public void mouseClicked(final MouseEvent arg0) {
                    int row = tableatipoutilizador.getSelectedRow();
                    String idpesq = (tableatipoutilizador.getModel()
                                                         .getValueAt(row, 0)).toString();
                    modelo.setPesquisa(idpesq);

                    MTipoUtilizadores modelorecebe = control.selecionaLista(modelo);
                    tipoUtilizadoresId.setText(String.valueOf(
                            modelorecebe.getTipoUtilizadoresId()));
                    descricao.setText(modelorecebe.getDescricao());
                    btneditar.setEnabled(true);
                    btnapagar.setEnabled(true);
                    btncancelar.setEnabled(true);
                }
            });
        preencheTabela("select * from tipoutilizador");
    }

    /**
     * The main method.
     *
     * @param args
     *          the arguments
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
                public void run() {
                    try {
                        VTipoUtilizador frame = new VTipoUtilizador();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir janela Utilizador! \n" + erro);
                    }
                }
            });
    }

    /**
     * Preenche tabela.
     *
     * @param sql
     *          the sql
     */
    public final void preencheTabela(final String sql) {
        ligacao.ligacaoDb();

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            tableatipoutilizador.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados no aarayList! \n" + ex);
        }

        ligacao.desligaDb();
    }
}
