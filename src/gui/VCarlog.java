/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package gui;

import dao.DaoUtilizadores;

import driver.ConnDb;

import model.MUtilizadores;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


// TODO: Auto-generated Javadoc
/**
 * The Class VCarlog.
 */
@SuppressWarnings("serial")
public class VCarlog extends JFrame {
    
    /** The modelo. */
    static MUtilizadores modelo = new MUtilizadores();

    /** The tipo utili. */
    static int tipoUtili;

    /** The control. */
    DaoUtilizadores control = new DaoUtilizadores();

    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The content pane. */
    private JPanel contentPane;

    /** The lbl new label. */
    private JLabel lblNewLabel;

    /** The tipouti. */
    private JLabel tipouti;

    /**
     * Instantiates a new v carlog.
     *
     * @param tipoUtili
     *          the tipo utili
     */
    public VCarlog(int tipoUtili) {
        int utilizador = tipoUtili;
        setTitle("Carlog By PedroRocha");
        ligacao.ligacaoDb();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 567, 485);

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu mnNewMenu = new JMenu("Administração");
        menuBar.add(mnNewMenu);

        JMenu mnNewMenu1 = new JMenu("Utilizadores");
        menuBar.add(mnNewMenu1);

        JMenuItem mntmNewMenuItem = new JMenuItem("Gerir Utilizadores");
        mntmNewMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    try {
                        VUtilizadores frame = new VUtilizadores();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir a janela Utilizadores! \n" + erro);
                    }
                }
            });

        mnNewMenu1.add(mntmNewMenuItem);

        JMenuItem mntmNewMenuItem2 = new JMenuItem(
                "Adicionar Tipo de Utilizadores");
        mntmNewMenuItem2.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    try {
                        VTipoUtilizador frame = new VTipoUtilizador();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir a janela Utilizadores! \n" + erro);
                    }
                }
            });
        mnNewMenu1.add(mntmNewMenuItem2);

        JMenu mnNewMenu3 = new JMenu("Automovel");
        menuBar.add(mnNewMenu3);

        JMenuItem mntmNewMenuItem3 = new JMenuItem("Gerir Automoveis");
        mntmNewMenuItem3.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    try {
                        VAutomovel frame = new VAutomovel();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir a janela \n" + erro);
                    }
                }
            });
        mnNewMenu3.add(mntmNewMenuItem3);

        JMenuItem mntmAbastecimentos = new JMenuItem("Abastecimentos");
        mntmAbastecimentos.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    try {
                        VAbastecimento frame = new VAbastecimento();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir a janela! \n" + erro);
                    }
                }
            });
        mnNewMenu3.add(mntmAbastecimentos);

        JMenuItem mntmNewMenuItem4 = new JMenuItem("Adicionar Tipo Combustivel");
        mntmNewMenuItem4.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    try {
                        VTipoCombustivel frame = new VTipoCombustivel();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir a janela! \n" + erro);
                    }
                }
            });
        mnNewMenu3.add(mntmNewMenuItem4);

        JMenu mnNewMenu4 = new JMenu("Revisões");
        menuBar.add(mnNewMenu4);

        JMenuItem mntmNewMenuItem5 = new JMenuItem("Gerir Revisoes");
        mntmNewMenuItem5.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    try {
                        VRevisoes frame = new VRevisoes();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir a janela! \n" + erro);
                    }
                }
            });
        mnNewMenu4.add(mntmNewMenuItem5);

        JMenuItem mntmNewMenuItem6 = new JMenuItem("Adicinar Serviços");
        mnNewMenu4.add(mntmNewMenuItem6);

        JMenuItem mntmNewMenuItem7 = new JMenuItem("Adicionar Peças");
        mnNewMenu4.add(mntmNewMenuItem7);

        JMenu mnNewMenu5 = new JMenu("Taxas");
        menuBar.add(mnNewMenu5);

        JMenuItem mntmNewMenuItem8 = new JMenuItem("Gerir Taxas");
        mntmNewMenuItem8.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    try {
                        VTaxaImposto frame = new VTaxaImposto();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir a janela! \n" + erro);
                    }
                }
            });

        mnNewMenu5.add(mntmNewMenuItem8);

        JMenuItem mntmNewMenuItem9 = new JMenuItem("Adicionar Taxas");
        mntmNewMenuItem9.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    try {
                        VTipoTaxa frame = new VTipoTaxa();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir a janela! \n" + erro);
                    }
                }
            });
        mnNewMenu5.add(mntmNewMenuItem9);

        JMenu mnNewMenu6 = new JMenu("Estatisticas");
        menuBar.add(mnNewMenu6);

        JMenu mnNewMenu2 = new JMenu("Sair");
        menuBar.add(mnNewMenu2);

        JMenuItem mntmNewMenuItem1 = new JMenuItem("Sair");
        mntmNewMenuItem1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    ligacao.desligaDb();
                    System.exit(0);
                }
            });
        mnNewMenu2.add(mntmNewMenuItem1);

        JMenuBar utilizadores = new JMenuBar();
        menuBar.add(utilizadores);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel logo = new JLabel("");
        logo.setBounds(53, 59, 452, 301);
        logo.setIcon(new ImageIcon(VCarlog.class.getResource(
                    "/imagens/09152f_3042d81f89e64dfc9dc9428249c55969.jpg")));
        contentPane.add(logo);
        lblNewLabel = new JLabel("Autenticado como :");
        lblNewLabel.setBounds(10, 394, 107, 14);
        contentPane.add(lblNewLabel);
        tipouti = new JLabel("");
        tipouti.setBounds(153, 394, 134, 14);
        contentPane.add(tipouti);

        if (utilizador == 3) {
            mnNewMenu.setEnabled(false);
            mntmNewMenuItem.setEnabled(false);
            mntmNewMenuItem2.setEnabled(false);
            mntmNewMenuItem3.setEnabled(false);
            mntmAbastecimentos.setEnabled(false);
            mntmNewMenuItem4.setEnabled(false);
            mntmNewMenuItem5.setEnabled(false);
            mntmNewMenuItem6.setEnabled(false);
            mntmNewMenuItem8.setEnabled(false);
            mntmNewMenuItem9.setEnabled(false);
            mntmNewMenuItem7.setEnabled(false);
            tipouti.setText("Convidado");
        } else if (utilizador == 2) {
            mnNewMenu.setEnabled(false);
            mntmNewMenuItem.setEnabled(false);
            mntmNewMenuItem2.setEnabled(false);
            mntmNewMenuItem4.setEnabled(false);
            mntmNewMenuItem6.setEnabled(false);
            mntmNewMenuItem7.setEnabled(false);
            mntmNewMenuItem9.setEnabled(false);
            tipouti.setText("Utilizador");
        } else if (utilizador == 1) {
            tipouti.setText("Administrador");
        }
    }

    /**
     * Launch the application.
     *
     * @param args
     *          the arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
                public void run() {
                    try {
                        VLogin frame = new VLogin();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir a janela principal! \n" + erro);
                    }
                }
            });
    }
}
