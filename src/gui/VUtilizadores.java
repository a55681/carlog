/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package gui;

import dao.DaoUtilizadores;

import driver.ConnDb;

import model.MUtilizadores;

import net.proteanit.sql.DbUtils;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;


// TODO: Auto-generated Javadoc
/**
 * The Class VUtilizadores.
 */
@SuppressWarnings("serial")
public class VUtilizadores extends JFrame {
    
    /** The modelo. */
    MUtilizadores modelo = new MUtilizadores();

    /** The control. */
    DaoUtilizadores control = new DaoUtilizadores();

    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The flag. */
    int flag = 0;

    /** The content pane. */
    private JPanel contentPane;

    /** The nome. */
    private JTextField nome;

    /** The password. */
    private JPasswordField password;

    /** The tableutilizadores. */
    private JTable tableutilizadores;

    /** The pesquisar. */
    private JTextField pesquisar;

    /** The jtipoutilizador. */
    private JComboBox<String> jtipoutilizador;

    /** The id. */
    private JTextField id;

    /**
     * Instantiates a new v utilizadores.
     */
    public VUtilizadores() {
        setBackground(Color.WHITE);
        setTitle("Gerir Utilizadores");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 541, 473);
        contentPane = new JPanel();
        contentPane.setBackground(Color.WHITE);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("Nome do Utilizador :");
        lblNewLabel.setBounds(159, 71, 121, 14);
        contentPane.add(lblNewLabel);

        JLabel lblNewLabel1 = new JLabel("Password :");
        lblNewLabel1.setBounds(159, 96, 121, 14);
        contentPane.add(lblNewLabel1);
        nome = new JTextField();
        nome.setEnabled(false);
        nome.setBounds(304, 68, 191, 20);
        contentPane.add(nome);
        nome.setColumns(10);

        JLabel lblNewLabel2 = new JLabel("Tipo de Utilizador :");
        lblNewLabel2.setBounds(159, 121, 121, 14);
        contentPane.add(lblNewLabel2);
        password = new JPasswordField();
        password.setEnabled(false);
        password.setBounds(304, 93, 191, 20);
        contentPane.add(password);
        jtipoutilizador = new JComboBox<String>();
        jtipoutilizador.setEnabled(false);
        jtipoutilizador.setBounds(304, 118, 191, 20);
        contentPane.add(jtipoutilizador);
        listacombo();

        JLabel lblNewLabel3 = new JLabel("New label");
        lblNewLabel3.setIcon(new ImageIcon(VUtilizadores.class.getResource(
                    "/imagens/1466017411_user.png")));
        lblNewLabel3.setBounds(10, 13, 124, 138);
        contentPane.add(lblNewLabel3);

        JButton btnNewButton1 = new JButton("");
        btnNewButton1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    dispose();
                }
            });

        btnNewButton1.setIcon(new ImageIcon(VUtilizadores.class.getResource(
                    "/imagens/Button-Close-icon.png")));
        btnNewButton1.setBounds(437, 383, 78, 41);
        contentPane.add(btnNewButton1);

        final JButton btnadicionar = new JButton("Adicionar");
        btnadicionar.setIcon(new ImageIcon(VUtilizadores.class.getResource(
                    "/imagens/Button-Add-icon.png")));
        btnadicionar.setBounds(10, 150, 124, 36);
        contentPane.add(btnadicionar);

        final JButton btnguardar = new JButton("Guardar");
        btnguardar.setEnabled(false);
        btnguardar.setIcon(new ImageIcon(VUtilizadores.class.getResource(
                    "/imagens/1466018858_floppy_disk_save.png")));
        btnguardar.setBounds(10, 197, 124, 36);
        contentPane.add(btnguardar);

        final JButton btncancelar = new JButton("Cancelar");
        btncancelar.setEnabled(false);
        btncancelar.setIcon(new ImageIcon(VUtilizadores.class.getResource(
                    "/imagens/Button-Cancel-icon.png")));
        btncancelar.setBounds(10, 244, 124, 36);
        contentPane.add(btncancelar);

        final JButton btneditar = new JButton("Editar");
        btneditar.setEnabled(false);
        btneditar.setIcon(new ImageIcon(VUtilizadores.class.getResource(
                    "/imagens/Text-Edit-icon.png")));
        btneditar.setBounds(10, 291, 124, 36);
        contentPane.add(btneditar);

        final JButton btnapagar = new JButton("Apagar");

        btnapagar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    int resposta = 0;
                    resposta = JOptionPane.showConfirmDialog(rootPane,
                            "Tem a Certeza que quer apagar?");

                    if (resposta == JOptionPane.YES_OPTION) {
                        modelo.setUtilizadorId((Integer.parseInt(id.getText())));
                        control.apagarDb(modelo);
                        nome.setText("");
                        password.setText("");
                        id.setText("");
                        pesquisar.setText("");
                        nome.setEnabled(false);
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnapagar.setEnabled(false);
                        btneditar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        password.setEnabled(false);
                        jtipoutilizador.setEnabled(false);
                        preencheTabela("select * from utilizadores");
                    }
                }
            });

        btnapagar.setEnabled(false);
        btnapagar.setIcon(new ImageIcon(VUtilizadores.class.getResource(
                    "/imagens/Button-Delete-icon.png")));
        btnapagar.setBounds(10, 338, 124, 36);
        contentPane.add(btnapagar);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(159, 150, 356, 222);
        contentPane.add(scrollPane);
        tableutilizadores = new JTable();
        scrollPane.setViewportView(tableutilizadores);
        tableutilizadores.setBorder(UIManager.getBorder(
                "Table.scrollPaneBorder"));
        pesquisar = new JTextField();
        pesquisar.setBounds(159, 13, 181, 20);
        contentPane.add(pesquisar);
        pesquisar.setColumns(10);

        JButton btnpesquisar = new JButton("Procurar");
        btnpesquisar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    modelo.setPesquisa(pesquisar.getText());

                    MUtilizadores modelorecebido = control.procuraUtilizadores(modelo);
                    id.setText(String.valueOf(modelo.getUtilizadorId()));
                    nome.setText(modelorecebido.getNome());
                    password.setText(modelorecebido.getPassword());
                    jtipoutilizador.setSelectedIndex(modelorecebido.getTipo() -
                        1);
                    btnadicionar.setEnabled(true);
                    btnguardar.setEnabled(false);
                    btncancelar.setEnabled(false);
                    btneditar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    preencheTabela(
                        "select * from utilizadores where nome like'%" +
                        modelo.getPesquisa() + "%'");
                }
            });

        btnpesquisar.setIcon(new ImageIcon(VUtilizadores.class.getResource(
                    "/imagens/search-button-icon.png")));
        btnpesquisar.setBounds(350, 13, 145, 23);
        contentPane.add(btnpesquisar);

        JLabel id2 = new JLabel("id :");
        id2.setBounds(159, 51, 46, 14);
        contentPane.add(id2);
        id = new JTextField();
        id.setEnabled(false);
        id.setBounds(304, 44, 191, 20);
        contentPane.add(id);
        id.setColumns(10);
        btnadicionar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    flag = 1;
                    btnguardar.setEnabled(true);
                    btncancelar.setEnabled(true);
                    nome.setEnabled(true);
                    password.setEnabled(true);
                    jtipoutilizador.setEnabled(true);
                    preencheTabela("select * from utilizadores");
                }
            });

        btneditar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    flag = 2;
                    btnguardar.setEnabled(true);
                    btncancelar.setEnabled(true);
                    btneditar.setEnabled(false);
                    btnadicionar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    nome.setEnabled(true);
                    password.setEnabled(true);
                    jtipoutilizador.setEnabled(true);
                    preencheTabela("select * from utilizadores");
                }
            });

        btncancelar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    btnguardar.setEnabled(!true);
                    btncancelar.setEnabled(!true);
                    btnadicionar.setEnabled(true);
                    btneditar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    pesquisar.setText("");
                    id.setText("");
                    nome.setText("");
                    password.setText("");
                    preencheTabela("select * from utilizadores");
                }
            });

        btnguardar.addActionListener(new ActionListener() {
                @SuppressWarnings("deprecation")
                public void actionPerformed(ActionEvent arg0) {
                    if (flag == 1) {
                        modelo.setNome(nome.getText());
                        modelo.setPassword(password.getText());
                        modelo.setTipo(jtipoutilizador.getSelectedIndex() + 1);
                        control.gravarDb(modelo);
                        nome.setText("");
                        password.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        nome.setEnabled(false);
                        password.setEnabled(false);
                        jtipoutilizador.setEnabled(false);
                        preencheTabela("select * from utilizadores");
                    } else {
                        modelo.setUtilizadorId((Integer.parseInt(id.getText())));
                        modelo.setNome(nome.getText());
                        modelo.setPassword(password.getText());
                        modelo.setTipo(jtipoutilizador.getSelectedIndex() + 1);
                        control.editarDb(modelo);
                        nome.setText("");
                        password.setText("");
                        id.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        nome.setEnabled(false);
                        password.setEnabled(false);
                        jtipoutilizador.setEnabled(false);
                        preencheTabela("select * from utilizadores");
                    }
                }
            });

        tableutilizadores.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent arg0) {
                    int row = tableutilizadores.getSelectedRow();
                    String idpesq = (tableutilizadores.getModel()
                                                      .getValueAt(row, 0)).toString();
                    modelo.setPesquisa(idpesq);

                    @SuppressWarnings("unused")
                    MUtilizadores model = control.selecionaLista(modelo);
                    id.setText(String.valueOf(modelo.getUtilizadorId()));
                    nome.setText(modelo.getNome());
                    password.setText(modelo.getPassword());
                    jtipoutilizador.setSelectedIndex(modelo.getTipo() - 1);
                    btneditar.setEnabled(true);
                    btnapagar.setEnabled(true);
                    btncancelar.setEnabled(true);
                }
            });
        listacombo();
        preencheTabela("select * from utilizadores");
    }

    /**
     * Listacombo.
     */
    public void listacombo() {
        ligacao.ligacaoDb();

        try {
            String sql = "select * from tipoutilizador";
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            jtipoutilizador.removeAllItems();

            while (rs.next()) {
                jtipoutilizador.addItem(rs.getString("descricao"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir dados na combo");
        }

        ligacao.desligaDb();
    }

    /**
     * The main method.
     *
     * @param args
     *          the arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
                public void run() {
                    try {
                        VUtilizadores frame = new VUtilizadores();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir janela Utilizadores! \n" + erro);
                    }
                }
            });
    }

    /**
     * Preenche tabela.
     *
     * @param sql
     *          the sql
     */
    public void preencheTabela(String sql) {
        // ArrayList dados = new ArrayList();
        // String[] colunas = new String[] { "id", "Nome", "Password", "Tipo de
        // Utilizador" };
        ligacao.ligacaoDb();

        // ligacao.executarsql(Sql);
        try {
            // String query = "select * from utilizadores";
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            tableutilizadores.setModel(DbUtils.resultSetToTableModel(rs));

            // while (ligacao.rs.next()) {
            // dados.add(new Object[] { ligacao.rs.getInt("UtilizadorID"),
            // ligacao.rs.getString("Nome"),
            // ligacao.rs.getString("Password"), ligacao.rs.getInt("Tipo") });
            // }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados no aarayList");
        }

        // MTabela table = new MTabela(dados, colunas);
        // tableutilizadores.setModel(table);
        // tableutilizadores.getColumnModel().getColumn(0).setPreferredWidth(23);
        // tableutilizadores.getColumnModel().getColumn(0).setResizable(false);
        // tableutilizadores.getColumnModel().getColumn(1).setPreferredWidth(180);
        // tableutilizadores.getColumnModel().getColumn(1).setResizable(false);
        // tableutilizadores.getColumnModel().getColumn(2).setPreferredWidth(80);
        // tableutilizadores.getColumnModel().getColumn(2).setResizable(false);
        // tableutilizadores.getColumnModel().getColumn(3).setPreferredWidth(80);
        // tableutilizadores.getColumnModel().getColumn(3).setResizable(false);
        // tableutilizadores.getTableHeader().setReorderingAllowed(false);
        // tableutilizadores.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        // tableutilizadores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ligacao.desligaDb();
    }
}
