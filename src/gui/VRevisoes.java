/*
 * Copyright (c) 2016 by Pedro Rocha.
 *
 * 23/jun/2016
 *
 */

package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import com.toedter.calendar.JDateChooser;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel;
import javax.swing.text.MaskFormatter;

import dao.DaoAutomovel;
import dao.DaoRevisoes;
import driver.ConnDb;
import model.MAutomovel;
import model.MRevisao;
import net.proteanit.sql.DbUtils;

// TODO: Auto-generated Javadoc

/**
 * The Class VRevisoes.
 */
public class VRevisoes extends JFrame {

  /** The Constant serialVersionUID. */
  private static final long   serialVersionUID = 1L;

  /** The modelocarro. */
  MAutomovel                  modelocarro      = new MAutomovel();

  /** The controlcarro. */
  DaoAutomovel                controlcarro     = new DaoAutomovel();

  /** The modelorevisao. */
  MRevisao                    modelorevisao    = new MRevisao();

  /** The controlrevisao. */
  DaoRevisoes                 controlrevisao   = new DaoRevisoes();

  /** The ligacao. */
  ConnDb                      ligacao          = new ConnDb();

  /** The flag. */
  int                         flag             = 0;

  /** The content pane. */
  private JPanel              contentPane;

  /** The marca. */
  private JTextField          marca;

  /** The modeloc. */
  private JTextField          modeloc;

  /** The tableautomovel. */
  private JTable              tableautomovel;

  /** The pesquisar. */
  private JTextField          pesquisar;

  /** The matricula. */
  private JFormattedTextField matricula;

  /** The automovel ID. */
  private JTextField          automovelID;

  /** The revisao ID. */
  private JTextField          revisaoID;

  /** The oficina. */
  private JTextField          oficina;

  /** The kmsveiculo. */
  private JTextField          kmsveiculo;

  /** The valor. */
  private JTextField          valor;

  /** The observacoes. */
  private JTextField          observacoes;

  /** The pesquisar 2. */
  private JTextField          pesquisar2;

  /** The table. */
  private JTable              table;

  /** The scroll pane 1. */
  private JScrollPane         scrollPane_1;

  /** The tiposervico. */
  private JComboBox<String>   tiposervico;

  /** The quantidade. */
  private JTextField          quantidade;

  /** The lbl quant. */
  private JLabel              lblQuant;

  /** The total. */
  private JTextField          total;

  /** The btn new button. */
  private JButton             btnNewButton;

  /** The btn new button 1. */
  private JButton             btnNewButton_1;

  /** The panel 1. */
  private JPanel              panel_1;

  /**
   * Create the frame.
   *
   * @throws ParseException
   *           the parse exception
   */
  public VRevisoes() throws ParseException {
    setResizable(false);
    setBackground(Color.WHITE);
    setTitle("Gerir Revisoes");
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 709, 533);
    contentPane = new JPanel();
    contentPane.setBackground(Color.WHITE);
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);

    JLabel lblNewLabel3 = new JLabel("");
    lblNewLabel3.setIcon(new ImageIcon(VRevisoes.class
        .getResource("/imagens/3d-man-fixing-tire-19487324.jpg")));
    lblNewLabel3.setBounds(10, 13, 118, 120);
    contentPane.add(lblNewLabel3);

    JButton btnNewButton1 = new JButton("Sair");
    btnNewButton1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        dispose();
      }
    });

    btnNewButton1.setIcon(new ImageIcon(
        VRevisoes.class.getResource("/imagens/Button-Close-icon.png")));
    btnNewButton1.setBounds(15, 447, 113, 41);
    contentPane.add(btnNewButton1);

    final JButton btnadicionar = new JButton("Adicionar");
    btnadicionar.setIcon(new ImageIcon(
        VRevisoes.class.getResource("/imagens/Button-Add-icon.png")));
    btnadicionar.setBounds(15, 172, 113, 36);
    contentPane.add(btnadicionar);

    final JButton btnguardar = new JButton("Guardar");
    btnguardar.setEnabled(false);
    btnguardar.setIcon(new ImageIcon(VRevisoes.class
        .getResource("/imagens/1466018858_floppy_disk_save.png")));
    btnguardar.setBounds(15, 219, 113, 36);
    contentPane.add(btnguardar);

    final JButton btncancelar = new JButton("Cancelar");
    btncancelar.setEnabled(false);
    btncancelar.setIcon(new ImageIcon(
        VRevisoes.class.getResource("/imagens/Button-Cancel-icon.png")));
    btncancelar.setBounds(15, 313, 113, 36);
    contentPane.add(btncancelar);

    final JButton btneditar = new JButton("Editar");
    btneditar.setEnabled(false);
    btneditar.setIcon(new ImageIcon(
        VRevisoes.class.getResource("/imagens/Text-Edit-icon.png")));
    btneditar.setBounds(15, 266, 113, 36);
    contentPane.add(btneditar);

    final JButton btnapagar = new JButton("Apagar");
    btnapagar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        int resposta = 0;
        resposta = JOptionPane.showConfirmDialog(rootPane,
            "Tem a Certeza que quer apagar?");

        if (resposta == JOptionPane.YES_OPTION) {
          modelorevisao.setAutomovelId((Integer.parseInt(revisaoID.getText())));
          controlrevisao.apagarDb(modelorevisao);
          automovelID.setText("");
          marca.setText("");
          modeloc.setText("");
          matricula.setText("");
          revisaoID.setText("");
          total.setText("");
          valor.setText("");
          observacoes.setText("");
          oficina.setText("");
          kmsveiculo.setText("");

          pesquisar.setText("");
          btnguardar.setEnabled(false);
          btncancelar.setEnabled(false);
          btnapagar.setEnabled(false);
          btneditar.setEnabled(false);
          btnadicionar.setEnabled(true);
          automovelID.setEnabled(false);
          marca.setEnabled(false);
          modeloc.setEnabled(false);
          matricula.setEnabled(false);
          revisaoID.setEnabled(false);

          valor.setEnabled(false);
          observacoes.setEnabled(false);
          oficina.setEnabled(false);
          kmsveiculo.setEnabled(false);
          pesquisar.setEnabled(false);
          preencheTabela(
              "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");

        }
      }
    });

    btnapagar.setEnabled(false);
    btnapagar.setIcon(new ImageIcon(
        VRevisoes.class.getResource("/imagens/Button-Delete-icon.png")));
    btnapagar.setBounds(15, 360, 113, 36);
    contentPane.add(btnapagar);

    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setBounds(416, 58, 274, 84);
    contentPane.add(scrollPane);
    tableautomovel = new JTable();
    tableautomovel.setFocusTraversalKeysEnabled(false);
    tableautomovel.setRowSelectionAllowed(false);
    scrollPane.setViewportView(tableautomovel);
    tableautomovel.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));

    observacoes = new JTextField();
    observacoes.setEnabled(false);
    observacoes.setBounds(226, 262, 180, 40);
    contentPane.add(observacoes);
    observacoes.setColumns(10);

    JPanel panel1 = new JPanel();
    panel1.setBorder(new TitledBorder(null, "Dados do Automovel",
        TitledBorder.LEADING, TitledBorder.TOP, null, null));
    panel1.setBounds(138, 13, 559, 137);
    contentPane.add(panel1);
    panel1.setLayout(null);

    JLabel id1 = new JLabel("ID :");
    id1.setBounds(10, 24, 18, 14);
    panel1.add(id1);
    automovelID = new JTextField();
    automovelID.setBounds(38, 21, 45, 20);
    panel1.add(automovelID);
    automovelID.setEnabled(false);
    automovelID.setColumns(10);

    JLabel lblNewLabel = new JLabel("Marca do Autumovel :");
    lblNewLabel.setBounds(10, 49, 121, 14);
    panel1.add(lblNewLabel);
    marca = new JTextField();
    marca.setBounds(141, 46, 121, 20);
    panel1.add(marca);
    marca.setEnabled(false);
    marca.setColumns(10);

    JLabel lblNewLabel1 = new JLabel("Modelo :");
    lblNewLabel1.setBounds(10, 80, 121, 14);
    panel1.add(lblNewLabel1);
    modeloc = new JTextField();
    modeloc.setBounds(141, 77, 121, 20);
    panel1.add(modeloc);
    modeloc.setEnabled(false);

    JLabel lblNewLabel5 = new JLabel("Matricula :");
    lblNewLabel5.setBounds(10, 111, 63, 14);
    panel1.add(lblNewLabel5);

    final MaskFormatter mask = new MaskFormatter("**-**-**");
    matricula = new JFormattedTextField();
    matricula.setBounds(183, 108, 78, 20);
    panel1.add(matricula);
    matricula.setHorizontalAlignment(SwingConstants.CENTER);
    matricula.setEnabled(false);
    mask.install(matricula);
    pesquisar = new JTextField();
    pesquisar.setBounds(319, 11, 139, 20);
    panel1.add(pesquisar);
    pesquisar.setColumns(10);

    // pesquisar carro
    JButton btnpesquisar = new JButton("");
    btnpesquisar.setBounds(484, 11, 53, 23);
    panel1.add(btnpesquisar);
    btnpesquisar.addActionListener(new ActionListener() {
      // pesquisa carro
      @Override
      public void actionPerformed(ActionEvent e) {
        modelocarro.setPesquisa(pesquisar.getText());
        modelocarro = controlcarro.procuraAutomovel(modelocarro);
        automovelID.setText(String.valueOf(modelocarro.getAutomovelId()));
        marca.setText(modelocarro.getMarca());
        modeloc.setText(modelocarro.getModelo());
        matricula.setText(modelocarro.getMatricula());
        btnadicionar.setEnabled(true);
        btnguardar.setEnabled(false);
        btncancelar.setEnabled(false);
        btneditar.setEnabled(false);
        btnapagar.setEnabled(false);
        preencheTabela("select * from abastecimento where local like'%"
            + modelocarro.getPesquisa() + "%'");
      }
    });

    btnpesquisar.setIcon(new ImageIcon(
        VRevisoes.class.getResource("/imagens/search-button-icon.png")));

    {
      panel_1 = new JPanel();
      panel_1.setBorder(new TitledBorder(null, "Dados da Revisão",
          TitledBorder.LEADING, TitledBorder.TOP, null, null));
      panel_1.setBounds(138, 153, 559, 157);
      contentPane.add(panel_1);
      panel_1.setLayout(null);

      final JDateChooser dataAb = new JDateChooser("dd/MM/yyyy", "##/##/####",
          '_');
      dataAb.setBounds(180, 21, 89, 23);
      panel_1.add(dataAb);

      JLabel lblNewLabel6 = new JLabel("Revisão em :");
      lblNewLabel6.setBounds(117, 21, 136, 14);
      panel_1.add(lblNewLabel6);
      revisaoID = new JTextField();
      revisaoID.setBounds(52, 21, 53, 20);
      panel_1.add(revisaoID);
      revisaoID.setEnabled(false);
      revisaoID.setColumns(10);

      JLabel lblNewLabel4 = new JLabel("ID :");
      lblNewLabel4.setBounds(10, 21, 46, 14);
      panel_1.add(lblNewLabel4);

      pesquisar2 = new JTextField();
      pesquisar2.setBounds(318, 21, 139, 20);
      panel_1.add(pesquisar2);
      pesquisar2.setColumns(10);

      JButton button = new JButton("");
      button.setBounds(478, 21, 58, 23);

      panel_1.add(button);
      button.setIcon(new ImageIcon(
          VRevisoes.class.getResource("/imagens/search-button-icon.png")));

      oficina = new JTextField();
      oficina.setBounds(125, 49, 144, 20);
      panel_1.add(oficina);
      oficina.setEnabled(false);
      oficina.setColumns(10);

      JLabel lblNewLabel7 = new JLabel("Oficina :");
      lblNewLabel7.setBounds(10, 52, 111, 14);
      panel_1.add(lblNewLabel7);

      kmsveiculo = new JTextField();
      kmsveiculo.setBounds(135, 80, 134, 20);
      panel_1.add(kmsveiculo);
      kmsveiculo.setEnabled(false);
      kmsveiculo.setColumns(10);

      JLabel lblAnoDeVenda = new JLabel("KmsVeiculo :");
      lblAnoDeVenda.setBounds(10, 77, 118, 14);
      panel_1.add(lblAnoDeVenda);

      JLabel lblNewLabel8 = new JLabel("Observações :");
      lblNewLabel8.setBounds(10, 102, 86, 14);
      panel_1.add(lblNewLabel8);

      JScrollPane scrollPane1 = new JScrollPane();
      scrollPane1.setBounds(275, 52, 274, 93);
      panel_1.add(scrollPane1);
    }

    JPanel panel_2 = new JPanel();
    panel_2.setBorder(new TitledBorder(null, "Detalhes Revisao",
        TitledBorder.LEADING, TitledBorder.TOP, null, null));
    panel_2.setBounds(138, 313, 559, 183);
    contentPane.add(panel_2);
    panel_2.setLayout(null);
    tiposervico = new JComboBox<String>();
    tiposervico.setBounds(90, 11, 97, 20);
    panel_2.add(tiposervico);
    tiposervico.setEnabled(false);
    listacombo();

    {
      JLabel lblNewLabe99 = new JLabel("Tipo de Serviço :");
      lblNewLabe99.setBounds(10, 14, 80, 14);
      panel_2.add(lblNewLabe99);
    }

    valor = new JTextField();
    valor.setBounds(342, 11, 86, 20);
    panel_2.add(valor);
    valor.setEnabled(false);
    valor.setColumns(10);

    JLabel lblNewLabel9 = new JLabel("Preço :");
    lblNewLabel9.setBounds(302, 14, 58, 14);
    panel_2.add(lblNewLabel9);
    quantidade = new JTextField();
    quantidade.setBounds(240, 11, 53, 20);
    panel_2.add(quantidade);
    quantidade.setColumns(10);
    lblQuant = new JLabel("Quant :");
    lblQuant.setBounds(196, 14, 46, 14);
    panel_2.add(lblQuant);
    scrollPane_1 = new JScrollPane();
    scrollPane_1.setBounds(10, 36, 414, 136);
    panel_2.add(scrollPane_1);

    table = new JTable();
    scrollPane_1.setViewportView(table);

    total = new JTextField();
    total.setBounds(469, 11, 80, 20);
    panel_2.add(total);
    total.setColumns(10);
    btnNewButton_1 = new JButton("Remover");
    btnNewButton_1.setBounds(424, 132, 125, 23);
    panel_2.add(btnNewButton_1);
    btnNewButton_1.setIcon(new ImageIcon(
        VRevisoes.class.getResource("/imagens/bullet_delete.png")));
    btnNewButton = new JButton("Adicionar");
    btnNewButton.setBounds(424, 62, 123, 23);
    panel_2.add(btnNewButton);
    btnNewButton.setIcon(
        new ImageIcon(VRevisoes.class.getResource("/imagens/bullet_add.png")));

    JLabel lblTotal = new JLabel("Total :");
    lblTotal.setBounds(434, 14, 46, 14);
    panel_2.add(lblTotal);

    btnadicionar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (automovelID.getText().contentEquals("")) {
          JOptionPane.showMessageDialog(null, "Deve Selecionar um carro! \n");
        } else {
          flag = 1;
          btnguardar.setEnabled(true);
          btncancelar.setEnabled(true);
          valor.setEnabled(true);
          observacoes.setEnabled(true);
          oficina.setEnabled(true);
          kmsveiculo.setEnabled(true);
          preencheTabela(
              "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
        }
      }
    });

    btneditar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        flag = 2;
        btnguardar.setEnabled(true);
        btncancelar.setEnabled(true);
        btneditar.setEnabled(false);
        btnadicionar.setEnabled(false);
        btnapagar.setEnabled(false);
        marca.setEnabled(true);
        modeloc.setEnabled(true);
        matricula.setEnabled(true);
        revisaoID.setEnabled(true);

        valor.setEnabled(true);
        observacoes.setEnabled(true);
        oficina.setEnabled(true);
        kmsveiculo.setEnabled(true);
        pesquisar.setEnabled(true);
        preencheTabela(
            "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
      }
    });

    btncancelar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnguardar.setEnabled(!true);
        btncancelar.setEnabled(!true);
        btnadicionar.setEnabled(true);
        btneditar.setEnabled(false);
        btnapagar.setEnabled(false);
        automovelID.setText("");
        marca.setText("");
        modeloc.setText("");
        matricula.setText("");
        revisaoID.setText("");

        valor.setText("");
        observacoes.setText("");
        oficina.setText("");
        kmsveiculo.setText("");
        pesquisar.setText("");
        preencheTabela(
            "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
      }
    });

    btnguardar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (flag == 1) {
          controlrevisao.gravarDb(modelorevisao);
          automovelID.setText("");
          marca.setText("");
          modeloc.setText("");
          matricula.setText("");
          revisaoID.setText("");

          valor.setText("");
          observacoes.setText("");
          oficina.setText("");
          kmsveiculo.setText("");
          pesquisar.setText("");
          btnguardar.setEnabled(false);
          btncancelar.setEnabled(false);
          btnadicionar.setEnabled(true);
          automovelID.setEnabled(false);
          marca.setEnabled(false);
          modeloc.setEnabled(false);
          matricula.setEnabled(false);
          revisaoID.setEnabled(false);

          valor.setEnabled(false);
          observacoes.setEnabled(false);
          oficina.setEnabled(false);
          kmsveiculo.setEnabled(false);
          pesquisar.setEnabled(false);
          preencheTabela(
              "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
        } else {
          modelorevisao.setAutomovelId(Integer.parseInt(automovelID.getText()));

          controlrevisao.editarDb(modelorevisao);
          automovelID.setText("");
          marca.setText("");
          modeloc.setText("");
          matricula.setText("");
          revisaoID.setText("");

          valor.setText("");
          observacoes.setText("");
          oficina.setText("");
          kmsveiculo.setText("");
          pesquisar.setText("");
          btnguardar.setEnabled(false);
          btncancelar.setEnabled(false);
          btnadicionar.setEnabled(true);
          automovelID.setEnabled(false);
          marca.setEnabled(false);
          modeloc.setEnabled(false);
          matricula.setEnabled(false);
          revisaoID.setEnabled(false);

          valor.setEnabled(false);
          observacoes.setEnabled(false);
          oficina.setEnabled(false);
          kmsveiculo.setEnabled(false);
          pesquisar.setEnabled(false);
          preencheTabela(
              "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
        }
      }
    });

    tableautomovel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent arg0) {
        int row = tableautomovel.getSelectedRow();
        String idpesq = (tableautomovel.getModel().getValueAt(row, 0))
            .toString();
        modelocarro.setPesquisa(idpesq);

        MAutomovel modelocarrorecebe = controlcarro.selecionaLista(modelocarro);
        automovelID.setText(String.valueOf(modelocarrorecebe.getAutomovelId()));
        marca.setText(modelocarrorecebe.getMarca());
        modeloc.setText(modelocarrorecebe.getModelo());
        matricula.setText(modelocarrorecebe.getMatricula());
        btneditar.setEnabled(true);
        btnapagar.setEnabled(true);
        btncancelar.setEnabled(true);
      }
    });
    listacombo();
    preencheTabela(
        "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
  }

  /**
   * Listacombo.
   */
  public void listacombo() {
    ligacao.ligacaoDb();

    try {
      String sql1 = ("select nome from TOperacao");
      PreparedStatement pst = ligacao.con.prepareStatement(sql1);
      ResultSet rs = pst.executeQuery();
      tiposervico.removeAllItems();

      while (rs.next()) {
        tiposervico.addItem(rs.getString("Nome"));
      }
    } catch (SQLException ex) {
      JOptionPane.showMessageDialog(null,
          "Erro ao inserir dados na combo! \n" + ex);
    }

    ligacao.desligaDb();
  }

  /**
   * The main method.
   *
   * @param args
   *          the arguments
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          VRevisoes frame = new VRevisoes();
          frame.setVisible(true);
        } catch (Exception erro) {
          JOptionPane.showMessageDialog(null,
              "Erro ao abrir janela revisoes \n" + erro);
        }
      }
    });
  }

  /**
   * Preenche tabela.
   *
   * @param sql2
   *          the sql 2
   */
  public void preencheTabela(String sql2) {
    ligacao.ligacaoDb();

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql2);
      ResultSet rs = pst.executeQuery();
      tableautomovel.setModel(DbUtils.resultSetToTableModel(rs));

      TableColumnModel tcm = tableautomovel.getColumnModel();
      tcm.getColumn(0).setHeaderValue("ID");
      tcm.getColumn(0).setPreferredWidth(0);
      tcm.getColumn(1).setHeaderValue("Marca");
      tcm.getColumn(2).setHeaderValue("Modelo");
      tcm.getColumn(3).setHeaderValue("Matricula");
    } catch (SQLException tcarro) {
      JOptionPane.showMessageDialog(null,
          "Erro ao inserir dados na tabela carro! \n" + tcarro);
    }

    ligacao.desligaDb();
  }
}
