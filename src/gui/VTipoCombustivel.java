/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package gui;

import dao.DaoTipoCombustivel;

import driver.ConnDb;

import model.MTipoCombustivel;

import net.proteanit.sql.DbUtils;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;


// TODO: Auto-generated Javadoc
/**
 * The Class VTipoCombustivel.
 */
@SuppressWarnings("serial")
public class VTipoCombustivel extends JFrame {
    
    /** The modelo. */
    MTipoCombustivel modelo = new MTipoCombustivel();

    /** The control. */
    DaoTipoCombustivel control = new DaoTipoCombustivel();

    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The flag. */
    int flag = 0;

    /** The content pane. */
    private JPanel contentPane;

    /** The table combustivel. */
    private JTable tableCombustivel;

    /** The pesquisar. */
    private JTextField pesquisar;

    /** The combustivel. */
    private JTextField combustivel;

    /**
     * Instantiates a new v automovel.
     *
     * @throws ParseException
     *           the parse exception
     */
    public VTipoCombustivel() throws ParseException {
        setResizable(false);
        setBackground(Color.WHITE);
        setTitle("Tipo Combustivel");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 723, 242);
        contentPane = new JPanel();
        contentPane.setBackground(Color.WHITE);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel3 = new JLabel("");
        lblNewLabel3.setIcon(new ImageIcon(VTipoCombustivel.class.getResource(
                    "/imagens/gas.jpg")));
        lblNewLabel3.setBounds(10, 13, 180, 138);
        contentPane.add(lblNewLabel3);

        JButton btnNewButton1 = new JButton("");
        btnNewButton1.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    dispose();
                }
            });

        btnNewButton1.setIcon(new ImageIcon(VTipoCombustivel.class.getResource(
                    "/imagens/Button-Close-icon.png")));
        btnNewButton1.setBounds(607, 162, 78, 36);
        contentPane.add(btnNewButton1);

        final JButton btnadicionar = new JButton("Adicionar");
        btnadicionar.setIcon(new ImageIcon(VTipoCombustivel.class.getResource(
                    "/imagens/Button-Add-icon.png")));
        btnadicionar.setBounds(31, 162, 113, 36);
        contentPane.add(btnadicionar);

        final JButton btnguardar = new JButton("Guardar");
        btnguardar.setEnabled(false);
        btnguardar.setIcon(new ImageIcon(VTipoCombustivel.class.getResource(
                    "/imagens/1466018858_floppy_disk_save.png")));
        btnguardar.setBounds(175, 162, 113, 36);
        contentPane.add(btnguardar);

        final JButton btncancelar = new JButton("Cancelar");
        btncancelar.setEnabled(false);
        btncancelar.setIcon(new ImageIcon(VTipoCombustivel.class.getResource(
                    "/imagens/Button-Cancel-icon.png")));
        btncancelar.setBounds(319, 162, 113, 36);
        contentPane.add(btncancelar);

        final JButton btnapagar = new JButton("Apagar");
        btnapagar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    int resposta = 0;
                    resposta = JOptionPane.showConfirmDialog(rootPane,
                            "Tem a Certeza que quer apagar?");

                    if (resposta == JOptionPane.YES_OPTION) {
                        modelo.setCombustivel(combustivel.getText());
                        control.apagarDb(modelo);
                        combustivel.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnapagar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        combustivel.setEnabled(false);
                        preencheTabela("select * from tipocombustivel");
                    }
                }
            });

        btnapagar.setEnabled(false);
        btnapagar.setIcon(new ImageIcon(VTipoCombustivel.class.getResource(
                    "/imagens/Button-Delete-icon.png")));
        btnapagar.setBounds(463, 162, 113, 36);
        contentPane.add(btnapagar);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(411, 61, 298, 89);
        contentPane.add(scrollPane);
        tableCombustivel = new JTable();
        scrollPane.setViewportView(tableCombustivel);
        tableCombustivel.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));
        pesquisar = new JTextField();
        pesquisar.setBounds(411, 27, 129, 20);
        contentPane.add(pesquisar);
        pesquisar.setColumns(10);

        JButton btnpesquisar = new JButton("Procurar");
        btnpesquisar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    modelo.setPesquisa(pesquisar.getText());

                    MTipoCombustivel modelorecebe = control.procuraCombustivel(modelo);
                    combustivel.setText(modelorecebe.getCombustivel());
                    btnadicionar.setEnabled(true);
                    btnguardar.setEnabled(false);
                    btncancelar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    preencheTabela(
                        "select * from tipocombustivel where combustivel like'%" +
                        modelo.getPesquisa() + "%'");
                }
            });

        btnpesquisar.setIcon(new ImageIcon(VTipoCombustivel.class.getResource(
                    "/imagens/search-button-icon.png")));
        btnpesquisar.setBounds(564, 26, 145, 23);
        contentPane.add(btnpesquisar);

        JLabel id1 = new JLabel("Nome da Taxa :");
        id1.setBounds(200, 71, 76, 14);
        contentPane.add(id1);
        combustivel = new JTextField();
        combustivel.setEnabled(false);
        combustivel.setBounds(286, 68, 86, 20);
        contentPane.add(combustivel);
        combustivel.setColumns(10);

        btnadicionar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    flag = 1;
                    combustivel.setEnabled(true);
                    btnguardar.setEnabled(true);
                    btncancelar.setEnabled(true);
                    preencheTabela("select * from tipocombustivel");
                }
            });
        btncancelar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    btnguardar.setEnabled(!true);
                    btncancelar.setEnabled(!true);
                    btnadicionar.setEnabled(true);
                    btnapagar.setEnabled(false);
                    combustivel.setText("");
                    pesquisar.setText("");
                    preencheTabela("select * from tipocombustivel");
                }
            });

        btnguardar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent arg0) {
                    if (flag == 1) {
                        modelo.setCombustivel(combustivel.getText());
                        control.gravarDb(modelo);
                        combustivel.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        combustivel.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela("select * from tipocombustivel");
                    } else {
                        modelo.setCombustivel(combustivel.getText());
                        control.editarDb(modelo);
                        combustivel.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        combustivel.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela("select * from tipocombustivel");
                    }
                }
            });

        tableCombustivel.addMouseListener(new MouseAdapter() {
                public void mouseClicked(final MouseEvent arg0) {
                    int row = tableCombustivel.getSelectedRow();
                    String idpesq = (tableCombustivel.getModel()
                                                     .getValueAt(row, 0)).toString();
                    modelo.setPesquisa(idpesq);

                    MTipoCombustivel modelorecebe = control.selecionaLista(modelo);
                    combustivel.setText(modelorecebe.getCombustivel());
                    btnapagar.setEnabled(true);
                    btncancelar.setEnabled(true);
                }
            });
        preencheTabela("select * from tipocombustivel");
    }

    /**
     * The main method.
     *
     * @param args
     *          the arguments
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
                public void run() {
                    try {
                        VTipoCombustivel frame = new VTipoCombustivel();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir janela Automovel! \n" + erro);
                    }
                }
            });
    }

    /**
     * Preenche tabela.
     *
     * @param sql
     *          the sql
     */
    public final void preencheTabela(final String sql) {
        ligacao.ligacaoDb();

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            tableCombustivel.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados no aarayList! \n" + ex);
        }

        ligacao.desligaDb();
    }
}
