/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package gui;

import com.toedter.calendar.JDateChooser;

import dao.DaoAbastecimento;
import dao.DaoAutomovel;
import dao.FormatRenderer;
import dao.NumberRenderer;

import driver.ConnDb;

import model.MAbastecimento;
import model.MAutomovel;

import net.proteanit.sql.DbUtils;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel;
import javax.swing.text.MaskFormatter;


// TODO: Auto-generated Javadoc
/**
 * The Class VAbastecimento.
 */
public class VAbastecimento extends JFrame {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The modelocarro. */
    MAutomovel modelocarro = new MAutomovel();

    /** The modelo abastecimento. */
    MAbastecimento modeloAbastecimento = new MAbastecimento();

    /** The controlcarro. */
    DaoAutomovel controlcarro = new DaoAutomovel();

    /** The controlabastecimento. */
    DaoAbastecimento controlabastecimento = new DaoAbastecimento();

    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The flag. */
    int flag = 0;

    /** The content pane. */
    private JPanel contentPane;

    /** The marca. */
    private JTextField marca;

    /** The modeloc. */
    private JTextField modeloc;

    /** The tableautomovel. */
    private JTable tableautomovel;

    /** The pesquisar. */
    private JTextField pesquisar;

    /** The matricula. */
    private JFormattedTextField matricula;

    /** The automovel id. */
    private JTextField automovelId;

    /** The abastecimento id. */
    private JTextField abastecimentoId;

    /** The local. */
    private JTextField local;

    /** The kmsveiculo. */
    private JTextField kmsveiculo;

    /** The litros. */
    private JTextField litros;

    /** The valor. */
    private JTextField valor;

    /** The combustivel. */
    private JTextField combustivel;

    /** The pesquisar ab. */
    private JTextField pesquisarAb;

    /** The tableabastecimento. */
    private JTable tableabastecimento;

    /**
     * Instantiates a new v abastecimento.
     *
     * @throws ParseException
     *           the parse exception
     */
    public VAbastecimento() throws ParseException {
        setResizable(false);
        setBackground(Color.WHITE);
        setTitle("Gerir Abastecimentos");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 757, 533);
        contentPane = new JPanel();
        contentPane.setBackground(Color.WHITE);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel3 = new JLabel("");
        lblNewLabel3.setBounds(10, 13, 171, 182);
        lblNewLabel3.setIcon(new ImageIcon(VAbastecimento.class.getResource(
                    "/imagens/gas.jpg")));
        contentPane.add(lblNewLabel3);

        JButton btnNewButton1 = new JButton("Sair");
        btnNewButton1.setBounds(584, 441, 113, 41);
        btnNewButton1.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    dispose();
                }
            });

        btnNewButton1.setIcon(new ImageIcon(VAbastecimento.class.getResource(
                    "/imagens/Button-Close-icon.png")));
        contentPane.add(btnNewButton1);

        final JButton btnadicionar = new JButton("Adicionar");
        btnadicionar.setBounds(34, 205, 113, 36);
        btnadicionar.setIcon(new ImageIcon(VAbastecimento.class.getResource(
                    "/imagens/Button-Add-icon.png")));
        contentPane.add(btnadicionar);

        final JButton btnguardar = new JButton("Guardar");
        btnguardar.setBounds(34, 252, 113, 36);
        btnguardar.setEnabled(false);
        btnguardar.setIcon(new ImageIcon(VAbastecimento.class.getResource(
                    "/imagens/1466018858_floppy_disk_save.png")));
        contentPane.add(btnguardar);

        final JButton btncancelar = new JButton("Cancelar");
        btncancelar.setBounds(34, 346, 113, 36);
        btncancelar.setEnabled(false);
        btncancelar.setIcon(new ImageIcon(VAbastecimento.class.getResource(
                    "/imagens/Button-Cancel-icon.png")));
        contentPane.add(btncancelar);

        final JButton btneditar = new JButton("Editar");
        btneditar.setBounds(34, 299, 113, 36);
        btneditar.setEnabled(false);
        btneditar.setIcon(new ImageIcon(VAbastecimento.class.getResource(
                    "/imagens/Text-Edit-icon.png")));
        contentPane.add(btneditar);

        final JButton btnapagar = new JButton("Apagar");
        btnapagar.setBounds(34, 393, 113, 36);
        btnapagar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    int resposta = 0;
                    resposta = JOptionPane.showConfirmDialog(rootPane,
                            "Tem a Certeza que quer apagar?");

                    if (resposta == JOptionPane.YES_OPTION) {
                        modeloAbastecimento.setAutomovelId((Integer.parseInt(
                                automovelId.getText())));
                        controlabastecimento.apagarDb(modeloAbastecimento);
                        automovelId.setText("");
                        marca.setText("");
                        modeloc.setText("");
                        matricula.setText("");
                        abastecimentoId.setText("");
                        litros.setText("");
                        valor.setText("");
                        local.setText("");
                        kmsveiculo.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnapagar.setEnabled(false);
                        btneditar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        automovelId.setEnabled(false);
                        marca.setEnabled(false);
                        modeloc.setEnabled(false);
                        matricula.setEnabled(false);
                        abastecimentoId.setEnabled(false);
                        litros.setEnabled(false);
                        valor.setEnabled(false);
                        local.setEnabled(false);
                        kmsveiculo.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela(
                            "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
                        preencheTabela2(
                            "select AbastecimentoID,Local,Data,Litros,Valor from abastecimento");
                    }
                }
            });

        btnapagar.setEnabled(false);
        btnapagar.setIcon(new ImageIcon(VAbastecimento.class.getResource(
                    "/imagens/Button-Delete-icon.png")));
        contentPane.add(btnapagar);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(457, 42, 284, 173);
        contentPane.add(scrollPane);
        tableautomovel = new JTable();
        tableautomovel.setFocusTraversalKeysEnabled(false);
        tableautomovel.setRowSelectionAllowed(false);
        scrollPane.setViewportView(tableautomovel);
        tableautomovel.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));

        pesquisar = new JTextField();
        pesquisar.setBounds(457, 11, 139, 20);
        contentPane.add(pesquisar);
        pesquisar.setColumns(10);

        // Pesquisa do Carro
        JButton btnpesquisar = new JButton("");
        btnpesquisar.setBounds(625, 11, 53, 23);
        btnpesquisar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    modelocarro.setPesquisa(pesquisar.getText());
                    modelocarro = controlcarro.procuraAutomovel(modelocarro);
                    automovelId.setText(String.valueOf(
                            modelocarro.getAutomovelId()));
                    marca.setText(modelocarro.getMarca());
                    modeloc.setText(modelocarro.getModelo());
                    matricula.setText(modelocarro.getMatricula());
                    combustivel.setText(modelocarro.getCombustivel());
                    btnadicionar.setEnabled(true);
                    btnguardar.setEnabled(false);
                    btncancelar.setEnabled(false);
                    btneditar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    preencheTabela("select * from automovel where marca like'%" +
                        modelocarro.getPesquisa() + "%'");
                }
            });

        btnpesquisar.setIcon(new ImageIcon(VAbastecimento.class.getResource(
                    "/imagens/search-button-icon.png")));
        contentPane.add(btnpesquisar);

        JLabel lblNewLabel4 = new JLabel("ID :");
        lblNewLabel4.setBounds(181, 224, 46, 14);
        contentPane.add(lblNewLabel4);
        abastecimentoId = new JTextField();
        abastecimentoId.setHorizontalAlignment(SwingConstants.CENTER);
        abastecimentoId.setBounds(206, 224, 46, 20);
        abastecimentoId.setEnabled(false);
        contentPane.add(abastecimentoId);
        abastecimentoId.setColumns(10);

        JLabel lblAnoDeVenda = new JLabel("KmsVeiculo :");
        lblAnoDeVenda.setBounds(181, 318, 139, 14);
        contentPane.add(lblAnoDeVenda);

        final MaskFormatter mask = new MaskFormatter("**-**-**");

        JLabel lblNewLabel6 = new JLabel("Abastecido em :");
        lblNewLabel6.setBounds(258, 227, 98, 14);
        contentPane.add(lblNewLabel6);

        JLabel lblNewLabel7 = new JLabel("Local do Abastecimento :");
        lblNewLabel7.setBounds(181, 267, 139, 23);
        contentPane.add(lblNewLabel7);
        local = new JTextField();
        local.setHorizontalAlignment(SwingConstants.CENTER);
        local.setBounds(334, 268, 113, 20);
        local.setEnabled(false);
        contentPane.add(local);
        local.setColumns(10);

        kmsveiculo = new JTextField();
        kmsveiculo.setHorizontalAlignment(SwingConstants.CENTER);
        kmsveiculo.setBounds(329, 312, 118, 20);
        kmsveiculo.setEnabled(false);
        contentPane.add(kmsveiculo);
        kmsveiculo.setColumns(10);

        litros = new JTextField();
        litros.setHorizontalAlignment(SwingConstants.CENTER);
        litros.setBounds(361, 362, 86, 20);
        litros.setEnabled(false);
        contentPane.add(litros);
        litros.setColumns(10);

        valor = new JTextField();
        valor.setHorizontalAlignment(SwingConstants.CENTER);
        valor.setBounds(361, 409, 86, 20);
        valor.setEnabled(false);
        contentPane.add(valor);
        valor.setColumns(10);

        JLabel lblNewLabel8 = new JLabel("Valor pago :");
        lblNewLabel8.setBounds(181, 412, 86, 14);
        contentPane.add(lblNewLabel8);

        JPanel panel1 = new JPanel();
        panel1.setBounds(181, 13, 266, 154);
        panel1.setBorder(new TitledBorder(null, "Dados do Automovel",
                TitledBorder.LEADING, TitledBorder.TOP, null, null));
        contentPane.add(panel1);
        panel1.setLayout(null);

        JLabel id1 = new JLabel("ID :");
        id1.setBounds(10, 24, 18, 14);
        panel1.add(id1);
        automovelId = new JTextField();
        automovelId.setHorizontalAlignment(SwingConstants.CENTER);
        automovelId.setBounds(38, 21, 45, 20);
        panel1.add(automovelId);
        automovelId.setEnabled(false);
        automovelId.setColumns(10);

        JLabel lblNewLabel = new JLabel("Marca do Autumovel :");
        lblNewLabel.setBounds(10, 49, 121, 14);
        panel1.add(lblNewLabel);
        marca = new JTextField();
        marca.setHorizontalAlignment(SwingConstants.CENTER);
        marca.setBounds(141, 46, 121, 20);
        panel1.add(marca);
        marca.setEnabled(false);
        marca.setColumns(10);

        JLabel lblNewLabel1 = new JLabel("Modelo :");
        lblNewLabel1.setBounds(10, 74, 121, 14);
        panel1.add(lblNewLabel1);
        modeloc = new JTextField();
        modeloc.setHorizontalAlignment(SwingConstants.CENTER);
        modeloc.setBounds(141, 71, 121, 20);
        panel1.add(modeloc);
        modeloc.setEnabled(false);

        JLabel lblNewLabel5 = new JLabel("Matricula :");
        lblNewLabel5.setBounds(10, 99, 63, 14);
        panel1.add(lblNewLabel5);

        matricula = new JFormattedTextField();
        matricula.setBounds(184, 96, 78, 20);
        panel1.add(matricula);
        matricula.setHorizontalAlignment(SwingConstants.CENTER);
        matricula.setEnabled(false);
        mask.install(matricula);

        JLabel lblNewLabel2 = new JLabel("Combustivel :");
        lblNewLabel2.setBounds(10, 124, 88, 14);
        panel1.add(lblNewLabel2);

        combustivel = new JTextField();
        combustivel.setHorizontalAlignment(SwingConstants.CENTER);
        combustivel.setEnabled(false);
        combustivel.setBounds(176, 121, 86, 20);
        panel1.add(combustivel);
        combustivel.setColumns(10);

        final JDateChooser dataAb = new JDateChooser("dd/MM/yyyy",
                "##/##/####", '_');

        dataAb.setBounds(358, 224, 89, 23);
        contentPane.add(dataAb);

        JLabel lblNewLabel9 = new JLabel("Litros Abastecidos :");
        lblNewLabel9.setBounds(181, 365, 139, 14);
        contentPane.add(lblNewLabel9);

        pesquisarAb = new JTextField();
        pesquisarAb.setBounds(457, 237, 139, 20);
        pesquisarAb.setColumns(10);
        contentPane.add(pesquisarAb);

        // Pesquisar abastecimento
        JButton btnpesquisarAb = new JButton("");
        btnpesquisarAb.setBounds(625, 234, 53, 23);
        btnpesquisarAb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    modeloAbastecimento.setPesquisa(pesquisar.getText());
                    modeloAbastecimento = controlabastecimento.procuraAbastecimento(modeloAbastecimento);
                    abastecimentoId.setText(String.valueOf(
                            modeloAbastecimento.getAutomovelId()));
                    dataAb.setDate(modeloAbastecimento.getData());
                    local.setText(modeloAbastecimento.getLocal());
                    kmsveiculo.setText(String.valueOf(
                            modeloAbastecimento.getKmsautomovel()));
                    valor.setText(String.valueOf(modeloAbastecimento.getValor()));
                    litros.setText(String.valueOf(
                            modeloAbastecimento.getLitros()));
                    btnadicionar.setEnabled(true);
                    btnguardar.setEnabled(false);
                    btncancelar.setEnabled(false);
                    btneditar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    preencheTabela(
                        "select * from abastecimento where local like'%" +
                        modelocarro.getPesquisa() + "%'");
                }
            });
        btnpesquisarAb.setIcon(new ImageIcon(VAbastecimento.class.getResource(
                    "/imagens/search-button-icon.png")));
        contentPane.add(btnpesquisarAb);

        JScrollPane scrollPane1 = new JScrollPane();
        scrollPane1.setBounds(457, 267, 284, 163);
        contentPane.add(scrollPane1);

        tableabastecimento = new JTable();
        tableabastecimento.setFocusTraversalKeysEnabled(false);
        tableabastecimento.setRowSelectionAllowed(false);
        scrollPane1.setViewportView(tableabastecimento);
        tableabastecimento.setBorder(UIManager.getBorder(
                "Table.scrollPaneBorder"));

        btnadicionar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    if (automovelId.getText().contentEquals("")) {
                        JOptionPane.showMessageDialog(null,
                            "Deve Selecionar um carro! \n");
                    } else {
                        flag = 1;
                        btnguardar.setEnabled(true);
                        btncancelar.setEnabled(true);
                        litros.setEnabled(true);
                        valor.setEnabled(true);
                        local.setEnabled(true);
                        kmsveiculo.setEnabled(true);
                        preencheTabela(
                            "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
                        preencheTabela2(
                            "select AbastecimentoID,Local,Data,Litros,Valor from abastecimento");
                    }
                }
            });

        btneditar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    flag = 2;
                    btnguardar.setEnabled(true);
                    btncancelar.setEnabled(true);
                    btneditar.setEnabled(false);
                    btnadicionar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    marca.setEnabled(true);
                    modeloc.setEnabled(true);
                    matricula.setEnabled(true);
                    abastecimentoId.setEnabled(true);
                    litros.setEnabled(true);
                    valor.setEnabled(true);
                    local.setEnabled(true);
                    kmsveiculo.setEnabled(true);
                    pesquisar.setEnabled(true);
                    preencheTabela(
                        "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
                    preencheTabela2(
                        "select AbastecimentoID,Local,Data,Litros,Valor from abastecimento");
                }
            });
        btncancelar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                    btnguardar.setEnabled(!true);
                    btncancelar.setEnabled(!true);
                    btnadicionar.setEnabled(true);
                    btneditar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    automovelId.setText("");
                    marca.setText("");
                    modeloc.setText("");
                    matricula.setText("");
                    dataAb.setDate(null);
                    abastecimentoId.setText("");
                    litros.setText("");
                    combustivel.setText("");
                    valor.setText("");
                    local.setText("");
                    kmsveiculo.setText("");
                    pesquisar.setText("");
                    preencheTabela(
                        "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
                    preencheTabela2(
                        "select AbastecimentoID,Local,Data,Litros,Valor from abastecimento");
                }
            });

        btnguardar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    if (flag == 1) {
                        modeloAbastecimento.setAutomovelId(Integer.parseInt(
                                automovelId.getText()));
                        modeloAbastecimento.setData(dataAb.getDate());
                        modeloAbastecimento.setLocal(local.getText());

                        if ((kmsveiculo.getText() != null) &&
                                !kmsveiculo.getText().isEmpty()) {
                            modeloAbastecimento.setKmsautomovel(Float.valueOf(
                                    kmsveiculo.getText()));
                        } else {
                            modeloAbastecimento.setKmsautomovel(String.valueOf(
                                    kmsveiculo.getText()));
                        }

                        if ((valor.getText() != null) &&
                                !valor.getText().isEmpty()) {
                            modeloAbastecimento.setValor(Float.valueOf(
                                    valor.getText()));
                        } else {
                            modeloAbastecimento.setValor(String.valueOf(
                                    valor.getText()));
                        }

                        if ((litros.getText() != null) &&
                                !litros.getText().isEmpty()) {
                            modeloAbastecimento.setLitros(Float.valueOf(
                                    litros.getText()));
                        } else {
                            modeloAbastecimento.setLitros(String.valueOf(
                                    litros.getText()));
                        }

                        controlabastecimento.gravarDb(modeloAbastecimento);
                        automovelId.setText("");
                        marca.setText("");
                        dataAb.setDate(null);
                        modeloc.setText("");
                        matricula.setText("");
                        abastecimentoId.setText("");
                        litros.setText("");
                        valor.setText("");
                        local.setText("");
                        kmsveiculo.setText("");
                        pesquisar.setText("");
                        dataAb.setDate(null);
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        automovelId.setEnabled(false);
                        marca.setEnabled(false);
                        modeloc.setEnabled(false);
                        matricula.setEnabled(false);
                        abastecimentoId.setEnabled(false);
                        litros.setEnabled(false);
                        valor.setEnabled(false);
                        local.setEnabled(false);
                        kmsveiculo.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela(
                            "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
                        preencheTabela2(
                            "select AbastecimentoID,Local,Data,Litros,Valor from abastecimento");
                    } else {
                        modeloAbastecimento.setAbastecimentoId(Integer.parseInt(
                                abastecimentoId.getText()));
                        modeloAbastecimento.setAutomovelId(Integer.parseInt(
                                automovelId.getText()));
                        modeloAbastecimento.setData(dataAb.getDate());
                        modeloAbastecimento.setLocal(local.getText());

                        if ((kmsveiculo.getText() != null) &&
                                !kmsveiculo.getText().isEmpty()) {
                            modeloAbastecimento.setKmsautomovel(Float.valueOf(
                                    kmsveiculo.getText()));
                        } else {
                            modeloAbastecimento.setKmsautomovel(String.valueOf(
                                    kmsveiculo.getText()));
                        }

                        if ((valor.getText() != null) &&
                                !valor.getText().isEmpty()) {
                            modeloAbastecimento.setValor(Float.valueOf(
                                    valor.getText()));
                        } else {
                            modeloAbastecimento.setValor(String.valueOf(
                                    valor.getText()));
                        }

                        if ((litros.getText() != null) &&
                                !litros.getText().isEmpty()) {
                            modeloAbastecimento.setLitros(Float.valueOf(
                                    litros.getText()));
                        } else {
                            modeloAbastecimento.setLitros(String.valueOf(
                                    litros.getText()));
                        }

                        controlabastecimento.editarDb(modeloAbastecimento);
                        automovelId.setText("");
                        marca.setText("");
                        dataAb.setDate(null);
                        modeloc.setText("");
                        matricula.setText("");
                        abastecimentoId.setText("");
                        litros.setText("");
                        valor.setText("");
                        local.setText("");
                        kmsveiculo.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        automovelId.setEnabled(false);
                        marca.setEnabled(false);
                        modeloc.setEnabled(false);
                        matricula.setEnabled(false);
                        abastecimentoId.setEnabled(false);
                        litros.setEnabled(false);
                        valor.setEnabled(false);
                        local.setEnabled(false);
                        kmsveiculo.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela(
                            "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
                        preencheTabela2(
                            "select AbastecimentoID,Local,Data,Litros,Valor from abastecimento");
                    }
                }
            });

        tableautomovel.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent arg0) {
                    int row = tableautomovel.getSelectedRow();
                    String idpesq = (tableautomovel.getModel().getValueAt(row, 0)).toString();
                    modelocarro.setPesquisa(idpesq);

                    MAutomovel modelocarrorecebe = controlcarro.selecionaLista(modelocarro);
                    automovelId.setText(String.valueOf(
                            modelocarrorecebe.getAutomovelId()));
                    marca.setText(modelocarrorecebe.getMarca());
                    modeloc.setText(modelocarrorecebe.getModelo());
                    matricula.setText(modelocarrorecebe.getMatricula());
                    combustivel.setText(modelocarrorecebe.getCombustivel());
                    btneditar.setEnabled(true);
                    btnapagar.setEnabled(true);
                    btncancelar.setEnabled(true);
                }
            });
        preencheTabela(
            "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
        preencheTabela2(
            "select AbastecimentoID,Local,Data,Litros,Valor from abastecimento");

        tableabastecimento.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent arg0) {
                    int row = tableabastecimento.getSelectedRow();
                    String idpesq = (tableabastecimento.getModel()
                                                       .getValueAt(row, 0)).toString();
                    modeloAbastecimento.setPesquisa(idpesq);

                    MAbastecimento modeloabastecimento = controlabastecimento.selecionaLista(modeloAbastecimento);
                    abastecimentoId.setText(String.valueOf(
                            modeloAbastecimento.getAbastecimentoId()));

                    String idpesq2 = (String.valueOf(modeloabastecimento.getAutomovelId()));
                    modelocarro.setPesquisa(idpesq2);

                    MAutomovel modelocarrorecebe = controlcarro.selecionaLista(modelocarro);
                    kmsveiculo.setText(String.valueOf(
                            modeloabastecimento.getKmsautomovel()));
                    automovelId.setText(String.valueOf(
                            modeloabastecimento.getAutomovelId()));
                    marca.setText(modelocarrorecebe.getMarca());
                    modeloc.setText(modelocarrorecebe.getModelo());
                    matricula.setText(modelocarrorecebe.getMatricula());
                    combustivel.setText(modelocarrorecebe.getCombustivel());
                    local.setText(modeloabastecimento.getLocal());
                    dataAb.setDate((modeloabastecimento.getData()));
                    litros.setText(String.valueOf(
                            modeloabastecimento.getLitros()));
                    valor.setText(String.valueOf(modeloabastecimento.getValor()));
                    btneditar.setEnabled(true);
                    btnapagar.setEnabled(true);
                    btncancelar.setEnabled(true);
                }
            });
        preencheTabela(
            "select AutomovelID,Marca,Modelo,Matricula,Combustivel from automovel");
        preencheTabela2(
            "select AbastecimentoID,Local,Data,Litros,Valor from abastecimento");
    }

    /**
     * The main method.
     *
     * @param args
     *          the arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
                public void run() {
                    try {
                        VAbastecimento frame = new VAbastecimento();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao instanciar a janela! \n" + erro);
                    }
                }
            });
    }

    /**
     * Preenche tabela.
     *
     * @param sql
     *          the sql
     */
    public void preencheTabela(String sql) {
        ligacao.ligacaoDb();

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            tableautomovel.setModel(DbUtils.resultSetToTableModel(rs));

            TableColumnModel tcm = tableautomovel.getColumnModel();
            tcm.getColumn(0).setHeaderValue("ID");
            tcm.getColumn(0).setPreferredWidth(0);
            tcm.getColumn(1).setHeaderValue("Marca");
            tcm.getColumn(2).setHeaderValue("Modelo");
            tcm.getColumn(3).setHeaderValue("Matricula");
            tcm.getColumn(4).setHeaderValue("Combustivel");
        } catch (SQLException tcarro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados na tabela carro! \n" + tcarro);
        }

        ligacao.desligaDb();
    }

    /**
     * Preenche tabela2.
     *
     * @param sql
     *          the sql
     */
    public void preencheTabela2(String sql) {
        ligacao.ligacaoDb();

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            tableabastecimento.setModel(DbUtils.resultSetToTableModel(rs));

            TableColumnModel tcm = tableabastecimento.getColumnModel();
            tcm.getColumn(0).setHeaderValue("ID");
            tcm.getColumn(0).setPreferredWidth(0);
            tcm.getColumn(1).setHeaderValue("Local");
            tcm.getColumn(2)
               .setCellRenderer(FormatRenderer.getDateTimeRenderer());
            tcm.getColumn(2).setHeaderValue("Data");
            tcm.getColumn(3).setHeaderValue("Litros");
            tcm.getColumn(4)
               .setCellRenderer(NumberRenderer.getCurrencyRenderer());
            tcm.getColumn(4).setHeaderValue("Valor");
        } catch (SQLException tabastece) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados na tabela abastecimento!\n" + tabastece);
        }

        ligacao.desligaDb();
    }
}
