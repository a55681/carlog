/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package gui;

import dao.DaoAutomovel;

import driver.ConnDb;

import model.MAutomovel;

import net.proteanit.sql.DbUtils;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;


// TODO: Auto-generated Javadoc
/**
 * The Class VAutomovel.
 */
@SuppressWarnings("serial")
public class VAutomovel extends JFrame {
    
    /** The modelo. */
    MAutomovel modelo = new MAutomovel();

    /** The control. */
    DaoAutomovel control = new DaoAutomovel();

    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The flag. */
    int flag = 0;

    /** The content pane. */
    private JPanel contentPane;

    /** The marca. */
    private JTextField marca;

    /** The modeloc. */
    private JTextField modeloc;

    /** The tableautomovel. */
    private JTable tableautomovel;

    /** The pesquisar. */
    private JTextField pesquisar;

    /** The combustivel. */
    private JComboBox<String> combustivel;

    /** The matricula. */
    private JFormattedTextField matricula;

    /** The automovel id. */
    private JTextField automovelId;

    /** The cor. */
    private JTextField cor;

    /** The cilindrada. */
    private JTextField cilindrada;

    /** The precoaquisicao. */
    private JTextField precoaquisicao;

    /** The precovenda. */
    private JTextField precovenda;

    /** The anoaquisicao. */
    private JTextField anoaquisicao;

    /** The anovenda. */
    private JTextField anovenda;

    /**
     * Instantiates a new v automovel.
     *
     * @throws ParseException
     *           the parse exception
     */
    public VAutomovel() throws ParseException {
        setResizable(false);
        setBackground(Color.WHITE);
        setTitle("Gerir Automoveis");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 730, 606);
        contentPane = new JPanel();
        contentPane.setBackground(Color.WHITE);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("Marca do Autumovel :");
        lblNewLabel.setBounds(290, 51, 121, 14);
        contentPane.add(lblNewLabel);

        JLabel lblNewLabel1 = new JLabel("Modelo :");
        lblNewLabel1.setBounds(159, 79, 121, 14);
        contentPane.add(lblNewLabel1);
        marca = new JTextField();
        marca.setEnabled(false);
        marca.setBounds(402, 48, 121, 20);
        contentPane.add(marca);
        marca.setColumns(10);

        JLabel lblNewLabel2 = new JLabel("Combustivel :");
        lblNewLabel2.setBounds(501, 79, 88, 14);
        contentPane.add(lblNewLabel2);

        modeloc = new JTextField();
        modeloc.setEnabled(false);
        modeloc.setBounds(210, 76, 121, 20);
        contentPane.add(modeloc);

        combustivel = new JComboBox<String>();
        combustivel.setEnabled(false);
        combustivel.setBounds(600, 76, 88, 20);
        contentPane.add(combustivel);
        listacombo();

        JLabel lblNewLabel3 = new JLabel("");
        lblNewLabel3.setIcon(new ImageIcon(VAutomovel.class.getResource(
                    "/imagens/bmw-mini-icon.png")));
        lblNewLabel3.setBounds(10, 13, 139, 138);
        contentPane.add(lblNewLabel3);

        JButton btnNewButton1 = new JButton("");
        btnNewButton1.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    dispose();
                }
            });

        btnNewButton1.setIcon(new ImageIcon(VAutomovel.class.getResource(
                    "/imagens/Button-Close-icon.png")));
        btnNewButton1.setBounds(620, 516, 78, 41);
        contentPane.add(btnNewButton1);

        final JButton btnadicionar = new JButton("Adicionar");
        btnadicionar.setIcon(new ImageIcon(VAutomovel.class.getResource(
                    "/imagens/Button-Add-icon.png")));
        btnadicionar.setBounds(10, 189, 124, 36);
        contentPane.add(btnadicionar);

        final JButton btnguardar = new JButton("Guardar");
        btnguardar.setEnabled(false);
        btnguardar.setIcon(new ImageIcon(VAutomovel.class.getResource(
                    "/imagens/1466018858_floppy_disk_save.png")));
        btnguardar.setBounds(144, 189, 121, 36);
        contentPane.add(btnguardar);

        final JButton btncancelar = new JButton("Cancelar");
        btncancelar.setEnabled(false);
        btncancelar.setIcon(new ImageIcon(VAutomovel.class.getResource(
                    "/imagens/Button-Cancel-icon.png")));
        btncancelar.setBounds(282, 189, 124, 36);
        contentPane.add(btncancelar);

        final JButton btneditar = new JButton("Editar");
        btneditar.setEnabled(false);
        btneditar.setIcon(new ImageIcon(VAutomovel.class.getResource(
                    "/imagens/Text-Edit-icon.png")));
        btneditar.setBounds(424, 189, 124, 36);
        contentPane.add(btneditar);

        final JButton btnapagar = new JButton("Apagar");
        btnapagar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    int resposta = 0;
                    resposta = JOptionPane.showConfirmDialog(rootPane,
                            "Tem a Certeza que quer apagar?");

                    if (resposta == JOptionPane.YES_OPTION) {
                        modelo.setAutomovelId((Integer.parseInt(
                                automovelId.getText())));
                        control.apagarDb(modelo);
                        automovelId.setText("");
                        marca.setText("");
                        modeloc.setText("");
                        matricula.setText("");
                        cor.setText("");
                        cilindrada.setText("");
                        anoaquisicao.setText("");
                        anovenda.setText("");
                        precoaquisicao.setText("");
                        precovenda.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnapagar.setEnabled(false);
                        btneditar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        automovelId.setEnabled(false);
                        marca.setEnabled(false);
                        modeloc.setEnabled(false);
                        matricula.setEnabled(false);
                        cor.setEnabled(false);
                        cilindrada.setEnabled(false);
                        anoaquisicao.setEnabled(false);
                        anovenda.setEnabled(false);
                        precoaquisicao.setEnabled(false);
                        precovenda.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela("select * from automovel");
                    }
                }
            });

        btnapagar.setEnabled(false);
        btnapagar.setIcon(new ImageIcon(VAutomovel.class.getResource(
                    "/imagens/Button-Delete-icon.png")));
        btnapagar.setBounds(574, 189, 124, 36);
        contentPane.add(btnapagar);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 236, 688, 269);
        contentPane.add(scrollPane);
        tableautomovel = new JTable();
        scrollPane.setViewportView(tableautomovel);
        tableautomovel.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));
        pesquisar = new JTextField();
        pesquisar.setBounds(159, 13, 181, 20);
        contentPane.add(pesquisar);
        pesquisar.setColumns(10);

        JButton btnpesquisar = new JButton("Procurar");
        btnpesquisar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    modelo.setPesquisa(pesquisar.getText());

                    MAutomovel modelorecebe = control.procuraAutomovel(modelo);
                    automovelId.setText(String.valueOf(
                            modelorecebe.getAutomovelId()));
                    marca.setText(modelorecebe.getMarca());
                    modeloc.setText(modelorecebe.getModelo());
                    cor.setText(modelorecebe.getCor());
                    cilindrada.setText(String.valueOf(
                            modelorecebe.getCilindrada()));
                    matricula.setText(modelorecebe.getMatricula());
                    anoaquisicao.setText(String.valueOf(
                            modelorecebe.getAnoAquisicao()));
                    anovenda.setText(String.valueOf(modelorecebe.getAnoVenda()));
                    precoaquisicao.setText(String.valueOf(
                            modelorecebe.getPrecoAquisicao()).toString());
                    precovenda.setText(String.valueOf(
                            modelorecebe.getPrecoVenda()).toString());
                    combustivel.setSelectedItem(modelorecebe.getCombustivel());
                    btnadicionar.setEnabled(true);
                    btnguardar.setEnabled(false);
                    btncancelar.setEnabled(false);
                    btneditar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    preencheTabela("select * from automovel where marca like'%" +
                        modelo.getPesquisa() + "%'");
                }
            });

        btnpesquisar.setIcon(new ImageIcon(VAutomovel.class.getResource(
                    "/imagens/search-button-icon.png")));
        btnpesquisar.setBounds(350, 13, 145, 23);
        contentPane.add(btnpesquisar);

        JLabel id1 = new JLabel("ID :");
        id1.setBounds(159, 51, 70, 14);
        contentPane.add(id1);
        automovelId = new JTextField();
        automovelId.setEnabled(false);
        automovelId.setBounds(210, 48, 70, 20);
        contentPane.add(automovelId);
        automovelId.setColumns(10);

        JLabel lblNewLabel4 = new JLabel("Cor :");
        lblNewLabel4.setBounds(543, 51, 46, 14);
        contentPane.add(lblNewLabel4);
        cor = new JTextField();
        cor.setEnabled(false);
        cor.setBounds(599, 48, 89, 20);
        contentPane.add(cor);
        cor.setColumns(10);

        JLabel lblCilindrada = new JLabel("Cilindrada :");
        lblCilindrada.setBounds(159, 104, 70, 14);
        contentPane.add(lblCilindrada);

        JLabel lblNewLabel5 = new JLabel("Matricula :");
        lblNewLabel5.setBounds(343, 79, 63, 14);
        contentPane.add(lblNewLabel5);

        JLabel lblAnoDeVenda = new JLabel("Ano de Venda :");
        lblAnoDeVenda.setBounds(159, 162, 147, 14);
        contentPane.add(lblAnoDeVenda);

        cilindrada = new JTextField();
        cilindrada.setEnabled(false);
        cilindrada.setBounds(220, 104, 86, 20);
        contentPane.add(cilindrada);
        cilindrada.setColumns(10);

        matricula = new JFormattedTextField();
        matricula.setEnabled(false);

        MaskFormatter mask = new MaskFormatter("**-**-**");
        mask.install(matricula);
        matricula.setBounds(402, 76, 78, 20);
        contentPane.add(matricula);

        JLabel lblNewLabel6 = new JLabel("Preço Aquisição");

        lblNewLabel6.setBounds(453, 137, 136, 14);
        contentPane.add(lblNewLabel6);

        JLabel lblNewLabel7 = new JLabel("Preço de Venda");
        lblNewLabel7.setBounds(453, 158, 136, 23);
        contentPane.add(lblNewLabel7);
        precoaquisicao = new JTextField();
        precoaquisicao.setEnabled(false);
        precoaquisicao.setBounds(600, 131, 86, 20);
        contentPane.add(precoaquisicao);
        precoaquisicao.setColumns(10);

        precovenda = new JTextField();
        precovenda.setEnabled(false);
        precovenda.setBounds(600, 158, 86, 20);
        contentPane.add(precovenda);
        precovenda.setColumns(10);

        anoaquisicao = new JTextField();
        anoaquisicao.setEnabled(false);
        anoaquisicao.setBounds(325, 131, 86, 20);
        contentPane.add(anoaquisicao);
        anoaquisicao.setColumns(10);

        anovenda = new JTextField();
        anovenda.setEnabled(false);
        anovenda.setBounds(320, 158, 86, 20);
        contentPane.add(anovenda);
        anovenda.setColumns(10);

        JLabel lblAnoDeCompra = new JLabel("Ano de Compra");
        lblAnoDeCompra.setBounds(159, 135, 147, 14);
        contentPane.add(lblAnoDeCompra);

        btnadicionar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    flag = 1;
                    btnguardar.setEnabled(true);
                    btncancelar.setEnabled(true);
                    preencheTabela("select * from automovel");
                }
            });

        btneditar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent action) {
                    flag = 2;
                    btnguardar.setEnabled(true);
                    btncancelar.setEnabled(true);
                    btneditar.setEnabled(false);
                    btnadicionar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    marca.setEnabled(true);
                    modeloc.setEnabled(true);
                    matricula.setEnabled(true);
                    cor.setEnabled(true);
                    cilindrada.setEnabled(true);
                    anoaquisicao.setEnabled(true);
                    anovenda.setEnabled(true);
                    precoaquisicao.setEnabled(true);
                    precovenda.setEnabled(true);
                    pesquisar.setEnabled(true);
                    preencheTabela("select * from automovel");
                }
            });
        btncancelar.addActionListener(new ActionListener() {
                /*
                 * (non-Javadoc)
                 *
                 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
                 */
                public void actionPerformed(final ActionEvent action) {
                    btnguardar.setEnabled(!true);
                    btncancelar.setEnabled(!true);
                    btnadicionar.setEnabled(true);
                    btneditar.setEnabled(false);
                    btnapagar.setEnabled(false);
                    automovelId.setText("");
                    marca.setText("");
                    modeloc.setText("");
                    matricula.setText("");
                    cor.setText("");
                    cilindrada.setText("");
                    anoaquisicao.setText("");
                    anovenda.setText("");
                    precoaquisicao.setText("");
                    precovenda.setText("");
                    pesquisar.setText("");
                    preencheTabela("select * from automovel");
                }
            });

        btnguardar.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent arg0) {
                    if (flag == 1) {
                        modelo.setMarca(marca.getText());
                        modelo.setModelo(modeloc.getText());
                        modelo.setCombustivel((String) combustivel.getSelectedItem());
                        modelo.setCor(cor.getText());
                        modelo.setCilindrada(Integer.parseInt(
                                cilindrada.getText()));
                        modelo.setMatricula(matricula.getText());
                        modelo.setAnoAquisicao(Integer.parseInt(
                                anoaquisicao.getText()));
                        modelo.setAnoVenda(Integer.parseInt(anovenda.getText()));

                        if ((precoaquisicao.getText() != null) &&
                                !precoaquisicao.getText().isEmpty()) {
                            modelo.setPrecoAquisicao(Float.valueOf(
                                    precoaquisicao.getText()));
                        } else {
                            modelo.setPrecoAquisicao(String.valueOf(
                                    precoaquisicao.getText()));
                        }

                        if ((precovenda.getText() != null) &&
                                !precovenda.getText().isEmpty()) {
                            modelo.setPrecoVenda(Float.valueOf(
                                    precovenda.getText()));
                        } else {
                            modelo.setPrecoVenda(String.valueOf(
                                    precovenda.getText()));
                        }

                        control.gravarDb(modelo);
                        automovelId.setText("");
                        marca.setText("");
                        modeloc.setText("");
                        matricula.setText("");
                        cor.setText("");
                        cilindrada.setText("");
                        anoaquisicao.setText("");
                        anovenda.setText("");
                        precoaquisicao.setText("");
                        precovenda.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        automovelId.setEnabled(false);
                        marca.setEnabled(false);
                        modeloc.setEnabled(false);
                        matricula.setEnabled(false);
                        cor.setEnabled(false);
                        cilindrada.setEnabled(false);
                        anoaquisicao.setEnabled(false);
                        anovenda.setEnabled(false);
                        precoaquisicao.setEnabled(false);
                        precovenda.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela("select * from automovel");
                    } else {
                        modelo.setAutomovelId(Integer.parseInt(
                                automovelId.getText()));
                        modelo.setMarca(marca.getText());
                        modelo.setModelo(modeloc.getText());
                        modelo.setCombustivel((String) combustivel.getSelectedItem());
                        modelo.setCor(cor.getText());
                        modelo.setCilindrada(String.valueOf(
                                cilindrada.getText()));
                        modelo.setMatricula(matricula.getText());
                        modelo.setAnoAquisicao(String.valueOf(
                                anoaquisicao.getText()));
                        modelo.setAnoVenda(String.valueOf(anovenda.getText()));

                        if ((precoaquisicao.getText() != null) &&
                                !precoaquisicao.getText().isEmpty()) {
                            modelo.setPrecoAquisicao(Float.valueOf(
                                    precoaquisicao.getText()));
                        } else {
                            modelo.setPrecoAquisicao(String.valueOf(
                                    precoaquisicao.getText()));
                        }

                        if ((precovenda.getText() != null) &&
                                !precovenda.getText().isEmpty()) {
                            modelo.setPrecoVenda(Float.valueOf(
                                    precovenda.getText()));
                        } else {
                            modelo.setPrecoVenda(String.valueOf(
                                    precovenda.getText()));
                        }

                        control.editarDb(modelo);
                        automovelId.setText("");
                        marca.setText("");
                        modeloc.setText("");
                        matricula.setText("");
                        cor.setText("");
                        cilindrada.setText("");
                        anoaquisicao.setText("");
                        anovenda.setText("");
                        precoaquisicao.setText("");
                        precovenda.setText("");
                        pesquisar.setText("");
                        btnguardar.setEnabled(false);
                        btncancelar.setEnabled(false);
                        btnadicionar.setEnabled(true);
                        automovelId.setEnabled(false);
                        marca.setEnabled(false);
                        modeloc.setEnabled(false);
                        matricula.setEnabled(false);
                        cor.setEnabled(false);
                        cilindrada.setEnabled(false);
                        anoaquisicao.setEnabled(false);
                        anovenda.setEnabled(false);
                        precoaquisicao.setEnabled(false);
                        precovenda.setEnabled(false);
                        pesquisar.setEnabled(false);
                        preencheTabela("select * from automovel");
                    }
                }
            });

        tableautomovel.addMouseListener(new MouseAdapter() {
                public void mouseClicked(final MouseEvent arg0) {
                    int row = tableautomovel.getSelectedRow();
                    String idpesq = (tableautomovel.getModel().getValueAt(row, 0)).toString();
                    modelo.setPesquisa(idpesq);

                    MAutomovel modelorecebe = control.selecionaLista(modelo);
                    automovelId.setText(String.valueOf(
                            modelorecebe.getAutomovelId()));
                    marca.setText(modelorecebe.getMarca());
                    modeloc.setText(modelorecebe.getModelo());
                    cor.setText(modelorecebe.getCor());
                    cilindrada.setText(String.valueOf(
                            modelorecebe.getCilindrada()));
                    matricula.setText(modelorecebe.getMatricula());
                    anoaquisicao.setText(String.valueOf(
                            modelorecebe.getAnoAquisicao()));
                    anovenda.setText(String.valueOf(modelorecebe.getAnoVenda()));
                    precoaquisicao.setText(String.valueOf(
                            (modelorecebe.getPrecoAquisicao())));
                    precovenda.setText(String.valueOf(
                            (modelorecebe.getPrecoVenda())));
                    combustivel.setSelectedItem(modelorecebe.getCombustivel());
                    btneditar.setEnabled(true);
                    btnapagar.setEnabled(true);
                    btncancelar.setEnabled(true);
                }
            });
        listacombo();
        preencheTabela("select * from automovel");
    }

    /**
     * Listacombo.
     */
    public final void listacombo() {
        ligacao.ligacaoDb();

        try {
            String sql = "select * from tipocombustivel";
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            combustivel.removeAllItems();

            while (rs.next()) {
                combustivel.addItem(rs.getString("combustivel"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir dados na combo");
        }

        ligacao.desligaDb();
    }

    /**
     * The main method.
     *
     * @param args
     *          the arguments
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
                public void run() {
                    try {
                        VAutomovel frame = new VAutomovel();
                        frame.setVisible(true);
                    } catch (Exception erro) {
                        JOptionPane.showMessageDialog(null,
                            "Erro ao abrir janela Automovel! \n" + erro);
                    }
                }
            });
    }

    /**
     * Preenche tabela.
     *
     * @param sql
     *          the sql
     */
    public final void preencheTabela(final String sql) {
        ligacao.ligacaoDb();

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            tableautomovel.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados no aarayList! \n" + ex);
        }

        ligacao.desligaDb();
    }
}
