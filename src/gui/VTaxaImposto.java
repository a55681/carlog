/*
 * Copyright (c) 2016 by Pedro Rocha.
 *
 * 23/jun/2016
 *
 */

package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import com.toedter.calendar.JDateChooser;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel;
import javax.swing.text.MaskFormatter;

import dao.DaoAutomovel;
import dao.DaoTaxaImposto;
import driver.ConnDb;
import model.MAutomovel;
import model.MTaxaImposto;
import net.proteanit.sql.DbUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class VTaxaImposto.
 */
@SuppressWarnings("serial")
public class VTaxaImposto extends JFrame {

  /** The modelo taxa. */
  MTaxaImposto                modeloTaxa         = new MTaxaImposto();
  
  /** The modelocarro. */
  MAutomovel                  modelocarro        = new MAutomovel();

  /** The control taxa imposto. */
  DaoTaxaImposto              controlTaxaImposto = new DaoTaxaImposto();
  
  /** The controlcarro. */
  DaoAutomovel                controlcarro       = new DaoAutomovel();

  /** The ligacao. */
  ConnDb                      ligacao            = new ConnDb();

  /** The flag. */
  int                         flag               = 0;

  /** The content pane. */
  private JPanel              contentPane;

  /** The marca. */
  private JTextField          marca;

  /** The modeloc. */
  private JTextField          modeloc;

  /** The tableautomovel. */
  private JTable              tableautomovel;

  /** The pesquisar. */
  private JTextField          pesquisar;

  /** The tipotaxa. */
  private JComboBox<String>   tipotaxa;

  /** The matricula. */
  private JFormattedTextField matricula;

  /** The automovel id. */
  private JTextField          automovelId;

  /** The taxaimposto ID. */
  private JTextField          taxaimpostoID;

  /** The valor. */
  private JTextField          valor;

  /** The datavalidade. */
  private JDateChooser        datavalidade;
  
  /** The data. */
  private JDateChooser        data;

  /** The panel. */
  private JPanel              panel;

  /** The label. */
  private JLabel              label;

  /** The label 1. */
  private JLabel              label_1;

  /** The label 2. */
  private JLabel              label_2;

  /** The label 3. */
  private JLabel              label_3;

  /** The panel 1. */
  private JPanel              panel_1;

  /** The panel 2. */
  private JPanel              panel_2;

  /** The text field. */
  private JTextField          textField;

  /** The btnpesquisar ab. */
  private JButton             btnpesquisarAb;

  /** The scroll pane 1. */
  private JScrollPane         scrollPane_1;
  
  /** The table taxa imposto. */
  private JTable              tableTaxaImposto;

  /**
   * Instantiates a new v automovel.
   *
   * @throws ParseException
   *           the parse exception
   */
  public VTaxaImposto() throws ParseException {
    setExtendedState(Frame.ICONIFIED);
    setResizable(false);
    setBackground(Color.WHITE);
    setTitle("Gerir Taxas/Impostos");
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 730, 435);
    contentPane = new JPanel();
    contentPane.setBackground(Color.WHITE);
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);

    JLabel lblNewLabel3 = new JLabel("");
    lblNewLabel3.setIcon(new ImageIcon(VTaxaImposto.class
        .getResource("/imagens/1466017395_preferences-contact-list.png")));
    lblNewLabel3.setBounds(10, 13, 139, 138);
    contentPane.add(lblNewLabel3);

    JButton btnNewButton1 = new JButton("");
    btnNewButton1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent action) {
        dispose();
      }
    });

    btnNewButton1.setIcon(new ImageIcon(
        VTaxaImposto.class.getResource("/imagens/Button-Close-icon.png")));
    btnNewButton1.setBounds(620, 353, 78, 41);
    contentPane.add(btnNewButton1);

    final JButton btnadicionar = new JButton("Adicionar");
    btnadicionar.setIcon(new ImageIcon(
        VTaxaImposto.class.getResource("/imagens/Button-Add-icon.png")));
    btnadicionar.setBounds(10, 162, 124, 36);
    contentPane.add(btnadicionar);

    final JButton btnguardar = new JButton("Guardar");
    btnguardar.setEnabled(false);
    btnguardar.setIcon(new ImageIcon(VTaxaImposto.class
        .getResource("/imagens/1466018858_floppy_disk_save.png")));
    btnguardar.setBounds(10, 210, 124, 36);
    contentPane.add(btnguardar);

    final JButton btncancelar = new JButton("Cancelar");
    btncancelar.setEnabled(false);
    btncancelar.setIcon(new ImageIcon(
        VTaxaImposto.class.getResource("/imagens/Button-Cancel-icon.png")));
    btncancelar.setBounds(10, 304, 124, 36);
    contentPane.add(btncancelar);

    final JButton btneditar = new JButton("Editar");
    btneditar.setEnabled(false);
    btneditar.setIcon(new ImageIcon(
        VTaxaImposto.class.getResource("/imagens/Text-Edit-icon.png")));
    btneditar.setBounds(10, 257, 124, 36);
    contentPane.add(btneditar);

    final JButton btnapagar = new JButton("Apagar");
    btnapagar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        int resposta = 0;
        resposta = JOptionPane.showConfirmDialog(rootPane,
            "Tem a Certeza que quer apagar?");

        if (resposta == JOptionPane.YES_OPTION) {
          modeloTaxa.setAutomovelId((Integer.parseInt(automovelId.getText())));
          controlTaxaImposto.apagarDb(modeloTaxa);
          automovelId.setText("");
          marca.setText("");
          modeloc.setText("");
          matricula.setText("");
          taxaimpostoID.setText("");
          valor.setText("");
          pesquisar.setText("");
          btnguardar.setEnabled(false);
          btncancelar.setEnabled(false);
          btnapagar.setEnabled(false);
          btneditar.setEnabled(false);
          btnadicionar.setEnabled(true);
          automovelId.setEnabled(false);
          marca.setEnabled(false);
          modeloc.setEnabled(false);
          matricula.setEnabled(false);
          taxaimpostoID.setEnabled(false);
          valor.setEnabled(false);
          pesquisar.setEnabled(false);
          preencheTabela(
              "select AutomovelID,Marca,Modelo,Matricula from automovel");
          // preencheTabela2(
          // "select taxaimpostoid,tipotaxa,Data,datavalidade,Valor from
          // taxaimposto");
        }
      }
    });

    btnapagar.setEnabled(false);
    btnapagar.setIcon(new ImageIcon(
        VTaxaImposto.class.getResource("/imagens/Button-Delete-icon.png")));
    btnapagar.setBounds(10, 353, 124, 36);
    contentPane.add(btnapagar);

    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setBounds(446, 66, 252, 85);
    contentPane.add(scrollPane);
    tableautomovel = new JTable();
    scrollPane.setViewportView(tableautomovel);
    tableautomovel.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));
    pesquisar = new JTextField();
    pesquisar.setBounds(484, 30, 131, 20);
    contentPane.add(pesquisar);
    pesquisar.setColumns(10);

    // Pesquisa do Carro
    JButton btnpesquisar = new JButton("");
    btnpesquisar.setBounds(625, 11, 53, 23);
    btnpesquisar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent action) {
        modelocarro.setPesquisa(pesquisar.getText());
        modelocarro = controlcarro.procuraAutomovel(modelocarro);
        automovelId.setText(String.valueOf(modelocarro.getAutomovelId()));
        marca.setText(modelocarro.getMarca());
        modeloc.setText(modelocarro.getModelo());
        matricula.setText(modelocarro.getMatricula());
        btnadicionar.setEnabled(true);
        btnguardar.setEnabled(false);
        btncancelar.setEnabled(false);
        btneditar.setEnabled(false);
        btnapagar.setEnabled(false);
        preencheTabela("select * from automovel where marca like'%"
            + modelocarro.getPesquisa() + "%'");
      }
    });

    btnpesquisar.setIcon(new ImageIcon(
        VTaxaImposto.class.getResource("/imagens/search-button-icon.png")));
    btnpesquisar.setBounds(645, 27, 53, 28);
    contentPane.add(btnpesquisar);

    MaskFormatter mask = new MaskFormatter("**-**-**");
    panel = new JPanel();
    panel.setLayout(null);
    panel.setBorder(new TitledBorder(null, "Dados do Automovel",
        TitledBorder.LEADING, TitledBorder.TOP, null, null));
    panel.setBounds(144, 30, 279, 121);
    contentPane.add(panel);
    label = new JLabel("ID :");
    label.setBounds(10, 24, 18, 14);
    panel.add(label);
    label_1 = new JLabel("Marca do Autumovel :");
    label_1.setBounds(10, 49, 121, 14);
    panel.add(label_1);
    label_2 = new JLabel("Modelo :");
    label_2.setBounds(10, 74, 121, 14);
    panel.add(label_2);
    label_3 = new JLabel("Matricula :");
    label_3.setBounds(10, 99, 63, 14);
    panel.add(label_3);
    automovelId = new JTextField();
    automovelId.setBounds(38, 21, 70, 20);
    panel.add(automovelId);
    automovelId.setEnabled(false);
    automovelId.setColumns(10);
    marca = new JTextField();
    marca.setBounds(141, 46, 121, 20);
    panel.add(marca);
    marca.setEnabled(false);
    marca.setColumns(10);

    modeloc = new JTextField();
    modeloc.setBounds(141, 71, 121, 20);
    panel.add(modeloc);
    modeloc.setEnabled(false);

    matricula = new JFormattedTextField();
    matricula.setBounds(184, 96, 78, 20);
    panel.add(matricula);
    matricula.setEnabled(false);
    mask.install(matricula);

    panel_2 = new JPanel();
    panel_2.setBorder(new TitledBorder(null, "Detalhes da Taxa",
        TitledBorder.LEADING, TitledBorder.TOP, null, null));
    panel_2.setBounds(144, 199, 281, 141);
    contentPane.add(panel_2);
    panel_2.setLayout(null);

    panel_1 = new JPanel();
    panel_1.setBounds(8, 16, 266, 114);
    panel_2.add(panel_1);
    panel_1.setLayout(null);

    JLabel lblNewLabel4 = new JLabel("ID :");
    lblNewLabel4.setBounds(10, 0, 18, 14);
    panel_1.add(lblNewLabel4);
    taxaimpostoID = new JTextField();
    taxaimpostoID.setBounds(38, 0, 67, 20);
    panel_1.add(taxaimpostoID);
    taxaimpostoID.setEnabled(false);
    taxaimpostoID.setColumns(10);

    JLabel lblNewLabel2 = new JLabel("Tipo Taxa :");
    lblNewLabel2.setBounds(115, 0, 54, 14);
    panel_1.add(lblNewLabel2);

    tipotaxa = new JComboBox<String>();
    tipotaxa.setBounds(179, 0, 88, 20);
    panel_1.add(tipotaxa);
    tipotaxa.setEnabled(false);
    listacombo();

    JLabel lblAnoDeCompra = new JLabel("Data");
    lblAnoDeCompra.setBounds(10, 31, 23, 14);
    panel_1.add(lblAnoDeCompra);

    data = new JDateChooser();
    data.setBounds(93, 25, 89, 20);
    panel_1.add(data);

    JLabel lblAnoDeVenda = new JLabel("Data Validade :");
    lblAnoDeVenda.setBounds(10, 62, 73, 14);
    panel_1.add(lblAnoDeVenda);

    datavalidade = new JDateChooser();
    datavalidade.setBounds(93, 56, 89, 20);
    panel_1.add(datavalidade);

    valor = new JTextField();
    valor.setBounds(96, 87, 86, 20);
    panel_1.add(valor);
    valor.setEnabled(false);
    valor.setColumns(10);

    JLabel lblCilindrada = new JLabel("Valor :");
    lblCilindrada.setBounds(10, 90, 31, 14);
    panel_1.add(lblCilindrada);
    textField = new JTextField();
    textField.setColumns(10);
    textField.setBounds(476, 168, 139, 20);
    contentPane.add(textField);

    // pesquisar taxas
    btnpesquisarAb = new JButton("");
    btnpesquisarAb.setIcon(new ImageIcon(
        VTaxaImposto.class.getResource("/imagens/search-button-icon.png")));
    btnpesquisarAb.setBounds(645, 168, 53, 23);
    btnpesquisarAb.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent action) {
        modeloTaxa.setPesquisa(pesquisar.getText());
        modeloTaxa = controlTaxaImposto.procuraTaxaImposto(modeloTaxa);
        taxaimpostoID.setText(String.valueOf(modeloTaxa.getAutomovelId()));
        data.setDate(modeloTaxa.getData());
        valor.setText(String.valueOf(modeloTaxa.getValor()));
        datavalidade.setDate(modeloTaxa.getData());
        tipotaxa.setSelectedItem(modeloTaxa.getTipotaxa());
        btnadicionar.setEnabled(true);
        btnguardar.setEnabled(false);
        btncancelar.setEnabled(false);
        btneditar.setEnabled(false);
        btnapagar.setEnabled(false);
        // preencheTabela2("select * from taxaimposto where tipotaxa like'%"
        // + modelocarro.getPesquisa() + "%'");

      }
    });
    contentPane.add(btnpesquisarAb);
    scrollPane_1 = new JScrollPane();
    scrollPane_1.setBounds(448, 202, 250, 138);

    contentPane.add(scrollPane_1);
    {
      tableTaxaImposto = new JTable();
      scrollPane_1.setViewportView(tableTaxaImposto);
    }

    btnadicionar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent action) {
        if (automovelId.getText().contentEquals("")) {
          JOptionPane.showMessageDialog(null, "Deve Selecionar um carro! \n");
        } else {
          flag = 1;
          btnguardar.setEnabled(true);
          btncancelar.setEnabled(true);
          taxaimpostoID.setEnabled(true);
          valor.setEnabled(true);
          data.setEnabled(true);
          datavalidade.setEnabled(true);
          preencheTabela(
              "select AutomovelID,Marca,Modelo,Matricula from automovel");
          // preencheTabela2(
          // "select taxaimpostoid,tipotaxa,Data,datavalidade,Valor from
          // taxaimposto");
        }
      }
    });

    btneditar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        flag = 2;
        btnguardar.setEnabled(true);
        btncancelar.setEnabled(true);
        btneditar.setEnabled(false);
        btnadicionar.setEnabled(false);
        btnapagar.setEnabled(false);
        marca.setEnabled(true);
        modeloc.setEnabled(true);
        matricula.setEnabled(true);
        taxaimpostoID.setEnabled(true);
        valor.setEnabled(true);
        pesquisar.setEnabled(true);
        preencheTabela(
            "select AutomovelID,Marca,Modelo,Matricula from automovel");
        // preencheTabela2(
        // "select taxaimpostoid,tipotaxa,Data,datavalidade,Valor from
        // taxaimposto");
      }
    });
    btncancelar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        btnguardar.setEnabled(!true);
        btncancelar.setEnabled(!true);
        btnadicionar.setEnabled(true);
        btneditar.setEnabled(false);
        btnapagar.setEnabled(false);
        automovelId.setText("");
        marca.setText("");
        modeloc.setText("");
        matricula.setText("");
        taxaimpostoID.setText("");
        valor.setText("");
        pesquisar.setText("");
        preencheTabela(
            "select AutomovelID,Marca,Modelo,Matricula from automovel");
        // preencheTabela2(
        // "select taxaimpostoid,tipotaxa,Data,datavalidade,Valor from
        // taxaimposto");
      }
    });

    btnguardar.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent arg0) {
        if (flag == 1) {
          modeloTaxa.setAutomovelId(Integer.parseInt(automovelId.getText()));
          modeloTaxa.setData(data.getDate());
          modeloTaxa.setTipotaxa((String) tipotaxa.getSelectedItem());
          modeloTaxa.setDataValidade(datavalidade.getDate());
          if ((valor.getText() != null) && !valor.getText().isEmpty()) {
            modeloTaxa.setValor(Float.valueOf(valor.getText()));
          } else {
            modeloTaxa.setValor(String.valueOf(valor.getText()));
          }
          controlTaxaImposto.gravarDb(modeloTaxa);
          automovelId.setText("");
          marca.setText("");
          modeloc.setText("");
          matricula.setText("");
          taxaimpostoID.setText("");
          valor.setText("");

          pesquisar.setText("");
          btnguardar.setEnabled(false);
          btncancelar.setEnabled(false);
          btnadicionar.setEnabled(true);
          automovelId.setEnabled(false);
          marca.setEnabled(false);
          modeloc.setEnabled(false);
          matricula.setEnabled(false);
          taxaimpostoID.setEnabled(false);
          valor.setEnabled(false);

          pesquisar.setEnabled(false);
          preencheTabela(
              "select AutomovelID,Marca,Modelo,Matricula from automovel");
          // preencheTabela2(
          // "select taxaimpostoid,tipotaxa,Data,datavalidade,Valor from
          // taxaimposto");
        } else {
          modeloTaxa.setAutomovelId(Integer.parseInt(automovelId.getText()));

          controlTaxaImposto.editarDb(modeloTaxa);
          automovelId.setText("");
          marca.setText("");
          modeloc.setText("");
          matricula.setText("");
          taxaimpostoID.setText("");
          valor.setText("");

          pesquisar.setText("");
          btnguardar.setEnabled(false);
          btncancelar.setEnabled(false);
          btnadicionar.setEnabled(true);
          automovelId.setEnabled(false);
          marca.setEnabled(false);
          modeloc.setEnabled(false);
          matricula.setEnabled(false);
          taxaimpostoID.setEnabled(false);
          valor.setEnabled(false);
          pesquisar.setEnabled(false);
          preencheTabela(
              "select AutomovelID,Marca,Modelo,Matricula from automovel");
          // preencheTabela2(
          // "select taxaimpostoid,tipotaxa,Data,datavalidade,Valor from
          // taxaimposto");
        }
      }
    });

    tableautomovel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent arg0) {
        int row = tableautomovel.getSelectedRow();
        String idpesq = (tableautomovel.getModel().getValueAt(row, 0))
            .toString();
        modelocarro.setPesquisa(idpesq);

        MAutomovel modelocarrorecebe = controlcarro.selecionaLista(modelocarro);
        automovelId.setText(String.valueOf(modelocarrorecebe.getAutomovelId()));
        marca.setText(modelocarrorecebe.getMarca());
        modeloc.setText(modelocarrorecebe.getModelo());
        matricula.setText(modelocarrorecebe.getMatricula());
        btneditar.setEnabled(true);
        btnapagar.setEnabled(true);
        btncancelar.setEnabled(true);
      }
    });
    listacombo();
    preencheTabela("select AutomovelID,Marca,Modelo,Matricula from automovel");
    // preencheTabela2(
    // "select taxaimpostoid,tipotaxa,Data,datavalidade,Valor from
    // taxaimposto");
  }

  /**
   * Listacombo.
   */
  public final void listacombo() {
    ligacao.ligacaoDb();
    try {
      String sql1 = "select * from tipotaxa";
      PreparedStatement pst = ligacao.con.prepareStatement(sql1);
      ResultSet rs = pst.executeQuery();
      tipotaxa.removeAllItems();

      while (rs.next()) {
        tipotaxa.addItem(rs.getString("nome"));
      }
    } catch (SQLException ex) {
      JOptionPane.showMessageDialog(null, "Erro ao inserir dados na combo");
    }

    ligacao.desligaDb();
  }

  /**
   * The main method.
   *
   * @param args
   *          the arguments
   */
  public static void main(final String[] args) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          VTaxaImposto frame = new VTaxaImposto();
          frame.setVisible(true);
        } catch (Exception erro) {
          JOptionPane.showMessageDialog(null,
              "Erro ao abrir janela Taxa Imposto! \n" + erro);
        }
      }
    });
  }

  /**
   * Preenche tabela.
   *
   * @param sql2
   *          the sql
   */
  public void preencheTabela(String sql2) {
    ligacao.ligacaoDb();

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql2);
      ResultSet rs = pst.executeQuery();
      tableautomovel.setModel(DbUtils.resultSetToTableModel(rs));

      TableColumnModel tcm = tableautomovel.getColumnModel();
      tcm.getColumn(0).setHeaderValue("ID");
      tcm.getColumn(0).setPreferredWidth(0);
      tcm.getColumn(1).setHeaderValue("Marca");
      tcm.getColumn(2).setHeaderValue("Modelo");
      tcm.getColumn(3).setHeaderValue("Matricula");
    } catch (SQLException tcarro) {
      JOptionPane.showMessageDialog(null,
          "Erro ao inserir dados na tabela carro! \n" + tcarro);
    }

    ligacao.desligaDb();
  }

}
