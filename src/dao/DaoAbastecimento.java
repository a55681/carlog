/*
 * Copyright (c) 2016 by Pedro Rocha.
 *
 * 23/jun/2016
 *
 */

package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import driver.ConnDb;
import model.MAbastecimento;

// TODO: Auto-generated Javadoc
/**
 * The Class DaoAbastecimento.
 */
public class DaoAbastecimento {

  /** The modelo abastecimento. */
  MAbastecimento modeloAbastecimento = new MAbastecimento();

  /** The ligacao. */
  ConnDb         ligacao             = new ConnDb();

  /**
   * Gravar db.
   *
   * @param modeloAbastecimento
   *          the modelo abastecimento
   */
  public final void gravarDb(final MAbastecimento modeloAbastecimento) {
    ligacao.ligacaoDb();

    String sql = "insert into abastecimento(AutomovelID,Data,Local,Kmsveiculo,valor,Litros"
        + ") values(?,?,?,?,?,?)";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setInt(1, modeloAbastecimento.getAutomovelId());
      pst.setDate(2,
          new java.sql.Date(modeloAbastecimento.getData().getTime()));
      pst.setString(3, modeloAbastecimento.getLocal());
      pst.setFloat(4, modeloAbastecimento.getKmsautomovel());
      pst.setFloat(5, modeloAbastecimento.getValor());
      pst.setFloat(6, modeloAbastecimento.getLitros());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao inserir dados \n" + erro);
    }

    ligacao.desligaDb();
  }

  /**
   * Editar db.
   *
   * @param modeloAbastecimento
   *          the modelo abastecimento
   */
  public final void editarDb(final MAbastecimento modeloAbastecimento) {
    ligacao.ligacaoDb();

    String sql = "update abastecimento set automovelID=?,data=?,local=?, kmsveiculo=?,"
        + "valor=?, litros=? where abastecimentoID=?";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setInt(1, modeloAbastecimento.getAutomovelId());
      pst.setDate(2,
          new java.sql.Date(modeloAbastecimento.getData().getTime()));
      pst.setString(3, modeloAbastecimento.getLocal());
      pst.setFloat(4, modeloAbastecimento.getKmsautomovel());
      pst.setFloat(5, modeloAbastecimento.getValor());
      pst.setFloat(6, modeloAbastecimento.getLitros());
      pst.setInt(7, modeloAbastecimento.getAbastecimentoId());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados alterados com sucesso");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao alterar dados \n" + erro);
    }

    ligacao.desligaDb();
  }

  /**
   * Apagar db.
   *
   * @param modeloAbastecimento
   *          the modelo abastecimento
   */
  public final void apagarDb(final MAbastecimento modeloAbastecimento) {
    ligacao.ligacaoDb();

    String sql = "delete from abastecimento where abastecimentoID=?";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setInt(1, modeloAbastecimento.getAbastecimentoId());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados apagados com sucesso");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao apagar dados! \n" + erro);
    }

    ligacao.desligaDb();
  }

  /**
   * Ler db.
   *
   * @param modeloAbastecimento
   *          the modelo abastecimento
   * @return the m abastecimento
   */
  public final MAbastecimento lerDb(final MAbastecimento modeloAbastecimento) {
    ligacao.ligacaoDb();

    String sql = "select * from abastecimento";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modeloAbastecimento.setAbastecimentoId(rs.getInt("abastecimentoID"));
        modeloAbastecimento.setAutomovelId(rs.getInt("automovelID"));
        modeloAbastecimento.setData(rs.getDate("data"));
        modeloAbastecimento.setLocal(rs.getString("local"));
        modeloAbastecimento.setKmsautomovel(rs.getFloat("kmsveiculo"));
        modeloAbastecimento.setValor(rs.getFloat("valor"));
        modeloAbastecimento.setLitros(rs.getFloat("litros"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao ler dados! \n" + erro);
    }

    ligacao.desligaDb();

    return modeloAbastecimento;
  }

  /**
   * Procura abastecimento.
   *
   * @param modeloAbastecimento
   *          the modelo abastecimento
   * @return the m abastecimento
   */
  public final MAbastecimento procuraAbastecimento(
      final MAbastecimento modeloAbastecimento) {
    ligacao.ligacaoDb();

    String sql = "select * from abastecimento where local like'%"
        + modeloAbastecimento.getPesquisa() + "%'";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modeloAbastecimento.setAbastecimentoId(rs.getInt("abastecimentoID"));
        modeloAbastecimento.setAutomovelId(rs.getInt("automovelID"));
        modeloAbastecimento.setData(rs.getDate("data"));
        modeloAbastecimento.setLocal(rs.getString("local"));
        modeloAbastecimento.setKmsautomovel(rs.getFloat("kmsveiculo"));
        modeloAbastecimento.setValor(rs.getFloat("valor"));
        modeloAbastecimento.setLitros(rs.getFloat("litros"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null,
          "N�o foi encontrado nenhum Automovel! \n" + erro);
    }

    ligacao.desligaDb();

    return modeloAbastecimento;
  }

  /**
   * Seleciona lista.
   *
   * @param modeloAbastecimento
   *          the modelo abastecimento
   * @return the m abastecimento
   */
  public final MAbastecimento selecionaLista(
      final MAbastecimento modeloAbastecimento) {
    ligacao.ligacaoDb();

    String sql = "select * from abastecimento where abastecimentoID ='"
        + modeloAbastecimento.getPesquisa() + "'";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modeloAbastecimento.setAbastecimentoId(rs.getInt("abastecimentoID"));
        modeloAbastecimento.setAutomovelId(rs.getInt("automovelID"));
        modeloAbastecimento.setData(rs.getDate("data"));
        modeloAbastecimento.setLocal(rs.getString("local"));
        modeloAbastecimento.setKmsautomovel(rs.getFloat("kmsveiculo"));
        modeloAbastecimento.setValor(rs.getFloat("valor"));
        modeloAbastecimento.setLitros(rs.getFloat("litros"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao procurar dados! \n" + erro);
    }

    ligacao.desligaDb();

    return modeloAbastecimento;
  }
}
