/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package dao;

import driver.ConnDb;

import model.MUtilizadores;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;


// TODO: Auto-generated Javadoc
/**
 * The Class DaoUtilizadores.
 */
public class DaoUtilizadores {
    
    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The modelo. */
    MUtilizadores modelo = new MUtilizadores();

    /**
     * Login db.
     *
     * @param modelo
     *          the modelo
     * @return true, if successful
     */
    public boolean loginDb(MUtilizadores modelo) {
        ligacao.ligacaoDb();

        boolean login = false;
        String sql = "select * from utilizadores where nome=? and password=?";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getNome());
            pst.setString(2, modelo.getPassword());

            ResultSet rs = pst.executeQuery();

            if (modelo.getNome().equals(rs.getString("nome")) &&
                    modelo.getPassword().equals(rs.getString("password"))) {
                modelo.setTipo(rs.getInt("tipo"));
                rs.close();
                pst.close();
                login = true;
            }
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao autenticar dados \n" + erro);
            login = false;
        }

        ligacao.desligaDb();

        return login;
    }

    /**
     * Apagar db.
     *
     * @param modelo
     *          the modelo
     */
    public void apagarDb(MUtilizadores modelo) {
        ligacao.ligacaoDb();

        String sql = "delete from utilizadores where utilizadorID=?";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);

            pst.setInt(1, modelo.getUtilizadorId());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados apagados com sucesso");
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "Erro ao apagar dados \n" +
                erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Editar db.
     *
     * @param modelo
     *          the modelo
     */
    public void editarDb(MUtilizadores modelo) {
        ligacao.ligacaoDb();

        String sql = "update utilizadores set nome=?,password=?,tipo=? where utilizadorID=?";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getNome());
            pst.setString(2, modelo.getPassword());
            pst.setInt(3, modelo.getTipo());
            pst.setInt(4, modelo.getUtilizadorId());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados alterados com sucesso");
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao alterar dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Gravar db.
     *
     * @param modelo
     *          the modelo
     */
    public void gravarDb(MUtilizadores modelo) {
        ligacao.ligacaoDb();

        String sql = "insert into utilizadores(nome,password,tipo) values(?,?,?)";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getNome());
            pst.setString(2, modelo.getPassword());
            pst.setInt(3, modelo.getTipo());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Ler db.
     *
     * @param modelo
     *          the modelo
     * @return the m utilizadores
     */
    public MUtilizadores lerDb(MUtilizadores modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from utilizadores";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setUtilizadorId(rs.getInt("utilizadorID"));
                modelo.setNome(rs.getString("nome"));
                modelo.setPassword(rs.getString("password"));
                modelo.setTipo(rs.getInt("tipo"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "Erro ao ler dados! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }

    /**
     * Procura utilizadores.
     *
     * @param modelo
     *          the modelo
     * @return the m utilizadores
     */
    public MUtilizadores procuraUtilizadores(MUtilizadores modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from utilizadores where nome like'%" +
            modelo.getPesquisa() + "%'";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setUtilizadorId(rs.getInt("utilizadorID"));
                modelo.setNome(rs.getString("nome"));
                modelo.setPassword(rs.getString("password"));
                modelo.setTipo(rs.getInt("tipo"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "N�o foi encontrado Utilizador! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }

    /**
     * Seleciona lista.
     *
     * @param modelo
     *          the modelo
     * @return the m utilizadores
     */
    public MUtilizadores selecionaLista(MUtilizadores modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from utilizadores where utilizadorID ='" +
            modelo.getPesquisa() + "'";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setUtilizadorId(rs.getInt("utilizadorID"));
                modelo.setNome(rs.getString("nome"));
                modelo.setPassword(rs.getString("password"));
                modelo.setTipo(rs.getInt("tipo"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao procurar dados! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }
}
