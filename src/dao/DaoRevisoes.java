/*
 * Copyright (c) 2016 by Pedro Rocha.
 *
 * 23/jun/2016
 *
 */

package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import driver.ConnDb;
import model.MAutomovel;
import model.MRevisao;
import model.MServicos;

// TODO: Auto-generated Javadoc
/**
 * The Class DaoRevisoes.
 */
public class DaoRevisoes {

  /** The modelorevisao. */
  MRevisao   modelorevisao    = new MRevisao();

  /** The modeloautomovel. */
  MAutomovel modeloautomovel  = new MAutomovel();

  /** The modeloservicos. */
  MServicos  modeloservicos   = new MServicos();

  /** The ligacaorevisao. */
  ConnDb     ligacaorevisao   = new ConnDb();

  /** The ligacaoautomovel. */
  ConnDb     ligacaoautomovel = new ConnDb();

  /** The ligacaodetalhes. */
  ConnDb     ligacaodetalhes  = new ConnDb();

  /**
   * Gravar db.
   *
   * @param modelorevisao
   *          the modelorevisao
   */
  public void gravarDb(MRevisao modelorevisao) {
    ligacaorevisao.ligacaoDb();

    String sql = "insert into revisao(AutomovelID,KmsVeiculo,Data,Observacoes,Oficina)"
        + " values(?,?,?,?)";

    try {
      PreparedStatement pst = ligacaorevisao.con.prepareStatement(sql);
      pst.setInt(1, modelorevisao.getAutomovelId());
      pst.setFloat(2, modelorevisao.getKmsautomovel());
      pst.setDate(3, new java.sql.Date(modelorevisao.getData().getTime()));
      pst.setString(4, modelorevisao.getObeservacoes());
      pst.setString(5, modelorevisao.getOficina());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao inserir dados \n" + erro);
    }

    ligacaorevisao.desligaDb();
  }

  /**
   * Editar db.
   *
   * @param modelorevisao
   *          the modelorevisao
   */
  public void editarDb(MRevisao modelorevisao) {
    ligacaorevisao.ligacaoDb();

    String sql = "update revisao set AutomovelID=?,KmsVeiculo=?,"
        + "Data=?, Observacoes=?,Oficina=? where automovelID=?";

    try {
      PreparedStatement pst = ligacaorevisao.con.prepareStatement(sql);
      pst.setInt(1, modelorevisao.getAutomovelId());
      pst.setFloat(2, modelorevisao.getKmsautomovel());
      pst.setDate(3, new java.sql.Date(modelorevisao.getData().getTime()));
      pst.setString(4, modelorevisao.getObeservacoes());
      pst.setString(5, modelorevisao.getOficina());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados alterados com sucesso");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao alterar dados \n" + erro);
    }

    ligacaorevisao.desligaDb();
  }

  /**
   * Apagar db.
   *
   * @param modelorevisao
   *          the modelorevisao
   */
  public void apagarDb(MRevisao modelorevisao) {
    ligacaorevisao.ligacaoDb();

    String sql = "delete from revisao where RevisaoID=?";

    try {
      PreparedStatement pst = ligacaorevisao.con.prepareStatement(sql);
      pst.setInt(1, modelorevisao.getAutomovelId());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados apagados com sucesso");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao apagar dados \n" + erro);
    }

    ligacaorevisao.desligaDb();
  }

  /**
   * Ler db.
   *
   * @param modelorevisao
   *          the modelorevisao
   * @return the m revisao
   */
  public MRevisao lerDb(MRevisao modelorevisao) {
    ligacaorevisao.ligacaoDb();
    String sql = "select * from revisao";
    try {
      PreparedStatement pst = ligacaorevisao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modelorevisao.setRevisaoId(rs.getInt("revisaoID"));
        modelorevisao.setAutomovelId(rs.getInt("automovelID"));
        modelorevisao.setKmsautomovel(rs.getFloat("modelo"));
        modelorevisao.setData(rs.getDate("cor"));
        modelorevisao.setObeservacoes(rs.getString("observacoes"));
        modelorevisao.setOficina(rs.getString("oficina"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao ler dados \n" + erro);
    }

    ligacaorevisao.desligaDb();

    return modelorevisao;
  }

  /**
   * Procura automovel.
   *
   * @param modelorevisao
   *          the modelorevisao
   * @return the m revisao
   */
  public MRevisao procuraAutomovel(MRevisao modelorevisao) {
    ligacaorevisao.ligacaoDb();

    String sql = "select * from revisao where marca like'%"
        + modelorevisao.getPesquisa() + "%'";

    try {
      PreparedStatement pst = ligacaorevisao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modelorevisao.setRevisaoId(rs.getInt("revisaoID"));
        modelorevisao.setAutomovelId(rs.getInt("automovelID"));
        modelorevisao.setKmsautomovel(rs.getFloat("modelo"));
        modelorevisao.setData(rs.getDate("cor"));
        modelorevisao.setObeservacoes(rs.getString("observacoes"));
        modelorevisao.setOficina(rs.getString("oficina"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null,
          "N�o foi encontrado nenhum Automovel  \n" + erro);
    }

    ligacaorevisao.desligaDb();

    return modelorevisao;
  }

  /**
   * Seleciona lista.
   *
   * @param modelorevisao
   *          the modelo
   * @return the m automovel
   */
  public MRevisao selecionaLista(MRevisao modelorevisao) {
    ligacaorevisao.ligacaoDb();

    String sql = "select * from revisao where revisaoID ='"
        + modelorevisao.getPesquisa() + "'";

    try {
      PreparedStatement pst = ligacaorevisao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modelorevisao.setRevisaoId(rs.getInt("revisaoID"));
        modelorevisao.setAutomovelId(rs.getInt("automovelID"));
        modelorevisao.setKmsautomovel(rs.getFloat("modelo"));
        modelorevisao.setData(rs.getDate("cor"));
        modelorevisao.setObeservacoes(rs.getString("observacoes"));
        modelorevisao.setOficina(rs.getString("oficina"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao procurar dados \n" + erro);
    }

    ligacaorevisao.desligaDb();

    return modelorevisao;
  }
}
