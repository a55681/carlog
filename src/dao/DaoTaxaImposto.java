/*
 * Copyright (c) 2016 by Pedro Rocha.
 *
 * 23/jun/2016
 *
 */

package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import driver.ConnDb;
import model.MTaxaImposto;

// TODO: Auto-generated Javadoc
/**
 * The Class DaoTaxaImposto.
 */
public class DaoTaxaImposto {

  /** The ligacao. */
  ConnDb       ligacao = new ConnDb();

  /** The modelo. */
  MTaxaImposto modelo  = new MTaxaImposto();

  /**
   * Gravar db.
   *
   * @param modelo
   *          the modelo
   */
  public void gravarDb(MTaxaImposto modelo) {
    ligacao.ligacaoDb();

    String sql = "insert into taxaimposto(automovelID,Data,valor,"
        + "datavalidade,tipotaxa) values(?,?,?,?,?)";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setInt(1, modelo.getAutomovelId());
      pst.setDate(2, new java.sql.Date(modelo.getData().getTime()));
      pst.setFloat(3, modelo.getValor());
      pst.setDate(4, new java.sql.Date(modelo.getDataValidade().getTime()));
      pst.setString(5, modelo.getTipotaxa());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao inserir dados! \n" + erro);
    }

    ligacao.desligaDb();
  }

  /**
   * Editar db.
   *
   * @param modelo
   *          the modelo
   */
  public void editarDb(MTaxaImposto modelo) {
    ligacao.ligacaoDb();

    String sql = "update taxaimposto set marca=?,modelo=?,cor=?, cilindrada=?,"
        + "matricula=?, combustivel=?, anoaquisicao=?, anovenda=?,"
        + "precoaquisicao=?,precovenda=? where taxaimpostoID=?";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setInt(1, modelo.getAutomovelId());
      pst.setDate(2, new java.sql.Date(modelo.getData().getTime()));
      pst.setFloat(3, modelo.getValor());
      pst.setDate(4, new java.sql.Date(modelo.getDataValidade().getTime()));
      pst.setString(5, modelo.getTipotaxa());
      pst.setInt(6, modelo.getTaxaImpostoId());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados alterados com sucesso!");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao alterar dados! \n" + erro);
    }

    ligacao.desligaDb();
  }

  /**
   * Apagar db.
   *
   * @param modelo
   *          the modelo
   */
  public void apagarDb(MTaxaImposto modelo) {
    ligacao.ligacaoDb();

    String sql = "delete from taxaimposto where taxaimpostoID=?";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setInt(1, modelo.getTaxaImpostoId());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados apagados com sucesso");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao apagar dados! \n" + erro);
    }

    ligacao.desligaDb();
  }

  /**
   * Ler db.
   *
   * @param modelo
   *          the modelo
   * @return the m automovel
   */
  public MTaxaImposto lerDb(MTaxaImposto modelo) {
    ligacao.ligacaoDb();

    String sql = "select * from taxaimposto";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modelo.setAutomovelId(rs.getInt("automovelID"));
        modelo.setTaxaImpostoId(rs.getInt("taxaimpostoID"));
        modelo.setData(rs.getDate("data"));
        modelo.setValor(rs.getFloat("valor"));
        modelo.setDataValidade(rs.getDate("datavalidade"));
        modelo.setTipotaxa(rs.getString("tipotaxa"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao ler dados! \n" + erro);
    }

    ligacao.desligaDb();

    return modelo;
  }

  /**
   * Procura taxa imposto.
   *
   * @param modelo
   *          the modelo
   * @return the m taxa imposto
   */
  public MTaxaImposto procuraTaxaImposto(MTaxaImposto modelo) {
    ligacao.ligacaoDb();

    String sql = "select * from taxaimposto where marca like'%"
        + modelo.getPesquisa() + "%'";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modelo.setAutomovelId(rs.getInt("automovelID"));
        modelo.setTaxaImpostoId(rs.getInt("taxaimpostoID"));
        modelo.setData(rs.getDate("data"));
        modelo.setValor(rs.getFloat("valor"));
        modelo.setDataValidade(rs.getDate("datavalidade"));
        modelo.setTipotaxa(rs.getString("tipotaxa"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null,
          "N�o foi encontrado nenhum taxa! \n" + erro);
    }

    ligacao.desligaDb();

    return modelo;
  }

  /**
   * Seleciona lista.
   *
   * @param modelo
   *          the modelo
   * @return the m automovel
   */
  public MTaxaImposto selecionaLista(MTaxaImposto modelo) {
    ligacao.ligacaoDb();

    String sql = "select * from taxaimposto where taxaimpostoID ='"
        + modelo.getPesquisa() + "'";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modelo.setAutomovelId(rs.getInt("automovelID"));
        modelo.setTaxaImpostoId(rs.getInt("taxaimpostoID"));
        modelo.setData(rs.getDate("data"));
        modelo.setValor(rs.getFloat("valor"));
        modelo.setDataValidade(rs.getDate("datavalidade"));
        modelo.setTipotaxa(rs.getString("tipotaxa"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao procurar dados! \n" + erro);
    }

    ligacao.desligaDb();

    return modelo;
  }

}
