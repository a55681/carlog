/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package dao;

import driver.ConnDb;

import model.MAutomovel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;


// TODO: Auto-generated Javadoc
/**
 * The Class DaoAutomovel.
 */
public class DaoAutomovel {
    
    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The modelo. */
    MAutomovel modelo = new MAutomovel();

    /**
     * Gravar db.
     *
     * @param modelo
     *          the modelo
     */
    public void gravarDb(MAutomovel modelo) {
        ligacao.ligacaoDb();

        String sql = "insert into automovel(marca,modelo,cor,cilindrada,matricula,combustivel," +
            "anoaquisicao,anovenda,precoaquisicao,precovenda) values(?,?,?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getMarca());
            pst.setString(2, modelo.getModelo());
            pst.setString(3, modelo.getCor());
            pst.setInt(4, modelo.getCilindrada());
            pst.setString(5, modelo.getMatricula());
            pst.setString(6, modelo.getCombustivel());
            pst.setInt(7, modelo.getAnoAquisicao());
            pst.setInt(8, modelo.getAnoVenda());
            pst.setDouble(9, modelo.getPrecoAquisicao());
            pst.setDouble(10, modelo.getPrecoVenda());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Editar db.
     *
     * @param modelo
     *          the modelo
     */
    public void editarDb(MAutomovel modelo) {
        ligacao.ligacaoDb();

        String sql = "update automovel set marca=?,modelo=?,cor=?, cilindrada=?," +
            "matricula=?, combustivel=?, anoaquisicao=?, anovenda=?," +
            "precoaquisicao=?,precovenda=? where automovelID=?";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getMarca());
            pst.setString(2, modelo.getModelo());
            pst.setString(3, modelo.getCor());
            pst.setInt(4, modelo.getCilindrada());
            pst.setString(5, modelo.getMatricula());
            pst.setString(6, modelo.getCombustivel());
            pst.setInt(7, modelo.getAnoAquisicao());
            pst.setInt(8, modelo.getAnoVenda());
            pst.setFloat(9, modelo.getPrecoAquisicao());
            pst.setFloat(10, modelo.getPrecoVenda());
            pst.setInt(11, modelo.getAutomovelId());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados alterados com sucesso!");
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao alterar dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Apagar db.
     *
     * @param modelo
     *          the modelo
     */
    public void apagarDb(MAutomovel modelo) {
        ligacao.ligacaoDb();

        String sql = "delete from automovel where automovelID=?";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setInt(1, modelo.getAutomovelId());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados apagados com sucesso");
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao apagar dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Ler db.
     *
     * @param modelo
     *          the modelo
     * @return the m automovel
     */
    public MAutomovel lerDb(MAutomovel modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from automovel";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setAutomovelId(rs.getInt("automovelID"));
                modelo.setMarca(rs.getString("marca"));
                modelo.setModelo(rs.getString("modelo"));
                modelo.setCor(rs.getString("cor"));
                modelo.setCilindrada(rs.getInt("cilindrada"));
                modelo.setMatricula(rs.getString("matricula"));
                modelo.setCombustivel(rs.getString("combustivel"));
                modelo.setAnoAquisicao(rs.getInt("anoaquisicao"));
                modelo.setAnoVenda(rs.getInt("anovenda"));
                modelo.setPrecoAquisicao(rs.getFloat("precoaquisicao"));
                modelo.setPrecoVenda(rs.getFloat("precovenda"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "Erro ao ler dados! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }

    /**
     * Procura automovel.
     *
     * @param modelo
     *          the modelo
     * @return the m automovel
     */
    public MAutomovel procuraAutomovel(MAutomovel modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from automovel where marca like'%" +
            modelo.getPesquisa() + "%'";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setAutomovelId(rs.getInt("automovelID"));
                modelo.setMarca(rs.getString("marca"));
                modelo.setModelo(rs.getString("modelo"));
                modelo.setCor(rs.getString("cor"));
                modelo.setCilindrada(rs.getInt("cilindrada"));
                modelo.setMatricula(rs.getString("matricula"));
                modelo.setCombustivel(rs.getString("combustivel"));
                modelo.setAnoAquisicao(rs.getInt("anoaquisicao"));
                modelo.setAnoVenda(rs.getInt("anovenda"));
                modelo.setPrecoAquisicao(rs.getFloat("precoaquisicao"));
                modelo.setPrecoVenda(rs.getFloat("precovenda"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "N�o foi encontrado nenhum Automovel! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }

    /**
     * Seleciona lista.
     *
     * @param modelo
     *          the modelo
     * @return the m automovel
     */
    public MAutomovel selecionaLista(MAutomovel modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from automovel where automovelID ='" +
            modelo.getPesquisa() + "'";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setAutomovelId(rs.getInt("automovelID"));
                modelo.setMarca(rs.getString("marca"));
                modelo.setModelo(rs.getString("modelo"));
                modelo.setCor(rs.getString("cor"));
                modelo.setCilindrada(rs.getInt("cilindrada"));
                modelo.setMatricula(rs.getString("matricula"));
                modelo.setCombustivel(rs.getString("combustivel"));
                modelo.setAnoAquisicao(rs.getInt("anoaquisicao"));
                modelo.setAnoVenda(rs.getInt("anovenda"));
                modelo.setPrecoAquisicao(rs.getFloat("precoaquisicao"));
                modelo.setPrecoVenda(rs.getFloat("precovenda"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao procurar dados! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }
}
