/*
 * Copyright (c) 2016 by Pedro Rocha.
 *
 * 23/jun/2016
 *
 */

package dao;

/**
 * @author Pedro
 *
 */
import java.text.NumberFormat;

import javax.swing.SwingConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class NumberRenderer.
 */
public class NumberRenderer extends FormatRenderer {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /*
   * Use the specified number formatter and right align the text
   */

  /**
   * Instantiates a new number renderer.
   *
   * @param formatter
   *          the formatter
   */
  public NumberRenderer(NumberFormat formatter) {
    super(formatter);
    setHorizontalAlignment(SwingConstants.RIGHT);
  }

  /**
   * Gets the currency renderer.
   *
   * @return the currency renderer
   */
  public static NumberRenderer getCurrencyRenderer() {
    return new NumberRenderer(NumberFormat.getCurrencyInstance());
  }

  /**
   * Gets the integer renderer.
   *
   * @return the integer renderer
   */
  public static NumberRenderer getIntegerRenderer() {
    return new NumberRenderer(NumberFormat.getIntegerInstance());
  }

  /**
   * Gets the percent renderer.
   *
   * @return the percent renderer
   */
  public static NumberRenderer getPercentRenderer() {
    return new NumberRenderer(NumberFormat.getPercentInstance());
  }
}
