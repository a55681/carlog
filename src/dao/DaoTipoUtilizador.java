/*
 * Copyright (c) 2016 by Pedro Rocha.
 *
 * 23/jun/2016
 *
 */

package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import driver.ConnDb;
import model.MTipoUtilizadores;

// TODO: Auto-generated Javadoc
/**
 * The Class DaoTipoUtilizador.
 */
public class DaoTipoUtilizador {

  /** The ligacao. */
  ConnDb            ligacao = new ConnDb();

  /** The modelo. */
  MTipoUtilizadores modelo  = new MTipoUtilizadores();

  /**
   * Gravar db.
   *
   * @param modelo
   *          the modelo
   */
  public void gravarDb(MTipoUtilizadores modelo) {
    ligacao.ligacaoDb();

    String sql = "insert into tipoutilizador(descricao) values(?)";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setString(1, modelo.getDescricao());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao inserir dados! \n" + erro);
    }

    ligacao.desligaDb();
  }

  /**
   * Apagar db.
   *
   * @param modelo
   *          the modelo
   */
  public void apagarDb(MTipoUtilizadores modelo) {
    ligacao.ligacaoDb();

    String sql = "delete from tipoutilizador where tipoutilizadorID=?";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setInt(1, modelo.getTipoUtilizadoresId());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao inserir dados! \n" + erro);
    }

    ligacao.desligaDb();
  }

  /**
   * Procuratipo.
   *
   * @param modelo
   *          the modelo
   * @return the m tipo utilizadores
   */
  public MTipoUtilizadores procuratipo(MTipoUtilizadores modelo) {
    ligacao.ligacaoDb();

    String sql = "select * tipoutilizador where descricao=?";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setString(1, modelo.getDescricao());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao inserir dados! \n" + erro);
    }

    ligacao.desligaDb();

    return modelo;
  }

  /**
   * Editar db.
   *
   * @param modelo
   *          the modelo
   * @return the m tipo utilizadores
   */
  public MTipoUtilizadores editarDb(MTipoUtilizadores modelo) {
    ligacao.ligacaoDb();

    String sql = "update tipoutilizador set tipoutilizadorID=?,"
        + "descricao=? where tipoutilizadorID=?";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      pst.setInt(1, modelo.getTipoUtilizadoresId());
      pst.setString(2, modelo.getDescricao());
      pst.setInt(3, modelo.getTipoUtilizadoresId());
      pst.execute();
      JOptionPane.showMessageDialog(null, "Dados alterados com sucesso");
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao alterar dados \n" + erro);
    }

    ligacao.desligaDb();

    return modelo;
  }

  /**
   * Seleciona lista.
   *
   * @param modelo
   *          the modelo
   * @return the m tipo utilizadores
   */
  public MTipoUtilizadores selecionaLista(MTipoUtilizadores modelo) {
    ligacao.ligacaoDb();

    String sql = "select * from tipoutilizador where tipoutilizadorID='"
        + modelo.getPesquisa() + "'";

    try {
      PreparedStatement pst = ligacao.con.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        modelo.setTipoUtilizadoresId(rs.getInt("tipoutilizadorID"));
        modelo.setDescricao(rs.getString("descricao"));
      }

      rs.close();
      pst.close();
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao procurar dados! \n" + erro);
    }

    ligacao.desligaDb();

    return modelo;
  }
}
