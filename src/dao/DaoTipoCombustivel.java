/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package dao;

import driver.ConnDb;

import model.MTipoCombustivel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;


// TODO: Auto-generated Javadoc
/**
 * The Class DaoTipoCombustivel.
 */
public class DaoTipoCombustivel {
    
    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The modelo. */
    MTipoCombustivel modelo = new MTipoCombustivel();

    /**
     * Gravar db.
     *
     * @param modelo
     *          the modelo
     */
    public void gravarDb(MTipoCombustivel modelo) {
        ligacao.ligacaoDb();

        String sql = "insert into tipocombustivel(combustivel) values(?)";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getCombustivel());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Apagar db.
     *
     * @param modelo
     *          the modelo
     */
    public void apagarDb(MTipoCombustivel modelo) {
        ligacao.ligacaoDb();

        String sql = "delete from tipocombustivel where combustivel=?";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getCombustivel());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Procura taxa.
     *
     * @param modelo
     *          the modelo
     * @return the m tipo taxa
     */
    public MTipoCombustivel procuraCombustivel(MTipoCombustivel modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from tipocombustivel where combustivel like'%" +
            modelo.getPesquisa() + "%'";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setCombustivel(rs.getString("combustivel"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "N�o foi encontrado nenhum Automovel! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }

    /**
     * Editar db.
     *
     * @param modelo
     *          the modelo
     * @return the m tipo taxa
     */
    public MTipoCombustivel editarDb(MTipoCombustivel modelo) {
        ligacao.ligacaoDb();

        String sql = "update tipocombustivel set combustivel=? where combustivel=?";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getCombustivel());
            pst.setString(2, modelo.getCombustivel());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados alterados com sucesso");
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao alterar dados \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }

    /**
     * Seleciona lista.
     *
     * @param modelo
     *          the modelo
     * @return the m tipo taxa
     */
    public MTipoCombustivel selecionaLista(MTipoCombustivel modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from tipocombustivel where combustivel='" +
            modelo.getPesquisa() + "'";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setCombustivel(rs.getString("combustivel"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao procurar dados! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }
}
