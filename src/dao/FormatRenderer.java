/*
 * Copyright (c) 2016 by Pedro Rocha.
 *
 * 23/jun/2016
 *
 */

package dao;

import java.text.DateFormat;
import java.text.Format;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;

// TODO: Auto-generated Javadoc
/*
 * Use a formatter to format the cell Object
 */

/**
 * The Class FormatRenderer.
 */
public class FormatRenderer extends DefaultTableCellRenderer {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The formatter. */
  private Format            formatter;

  /**
   * Instantiates a new format renderer.
   *
   * @param formatter
   *          the formatter
   */
  public FormatRenderer(Format formatter) {
    this.formatter = formatter;
  }

  /*
   * (non-Javadoc)
   *
   * @see javax.swing.table.DefaultTableCellRenderer#setValue(java.lang.Object)
   */
  @Override
  public void setValue(Object value) {
    try {
      if (value != null) {
        value = formatter.format(value);
      }
    } catch (IllegalArgumentException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao formatar objecto \n" + erro);
    }

    super.setValue(value);
  }

  /*
   * Use the default date/time formatter for the default locale
   */

  /**
   * Gets the date time renderer.
   *
   * @return the date time renderer
   */
  public static FormatRenderer getDateTimeRenderer() {
    return new FormatRenderer(DateFormat.getDateInstance(DateFormat.DEFAULT));
  }

  /*
   * Use the default time formatter for the default locale
   */

  /**
   * Gets the time renderer.
   *
   * @return the time renderer
   */
  public static FormatRenderer getTimeRenderer() {
    return new FormatRenderer(DateFormat.getTimeInstance());
  }
}
