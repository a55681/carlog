/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package dao;

import driver.ConnDb;

import model.MTipoTaxa;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;


// TODO: Auto-generated Javadoc
/**
 * The Class DaoTipoTaxa.
 */
public class DaoTipoTaxa {
    
    /** The ligacao. */
    ConnDb ligacao = new ConnDb();

    /** The modelo. */
    MTipoTaxa modelo = new MTipoTaxa();

    /**
     * Gravar db.
     *
     * @param modelo
     *          the modelo
     */
    public void gravarDb(MTipoTaxa modelo) {
        ligacao.ligacaoDb();

        String sql = "insert into tipotaxa(nome) values(?)";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getNome());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Apagar db.
     *
     * @param modelo
     *          the modelo
     */
    public void apagarDb(MTipoTaxa modelo) {
        ligacao.ligacaoDb();

        String sql = "delete from tipotaxa where nome=?";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getNome());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Ler db.
     *
     * @param modelo
     *          the modelo
     */
    public void lerDb(MTipoTaxa modelo) {
        ligacao.ligacaoDb();

        String sql = "insert into tipotaxa(nome) values(?)";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getNome());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao inserir dados! \n" + erro);
        }

        ligacao.desligaDb();
    }

    /**
     * Procura taxa.
     *
     * @param modelo
     *          the modelo
     * @return the m tipo taxa
     */
    public MTipoTaxa procuraTaxa(MTipoTaxa modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from tipotaxa where nome like'%" +
            modelo.getPesquisa() + "%'";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setNome(rs.getString("nome"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "N�o foi encontrado nenhum Automovel! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }

    /**
     * Editar db.
     *
     * @param modelo
     *          the modelo
     * @return the m tipo taxa
     */
    public MTipoTaxa editarDb(MTipoTaxa modelo) {
        ligacao.ligacaoDb();

        String sql = "update tipotaxa set nome=? where nome=?";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            pst.setString(1, modelo.getNome());
            pst.setString(2, modelo.getNome());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados alterados com sucesso");
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao alterar dados \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }

    /**
     * Seleciona lista.
     *
     * @param modelo
     *          the modelo
     * @return the m tipo taxa
     */
    public MTipoTaxa selecionaLista(MTipoTaxa modelo) {
        ligacao.ligacaoDb();

        String sql = "select * from tipotaxa where nome='" +
            modelo.getPesquisa() + "'";

        try {
            PreparedStatement pst = ligacao.con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                modelo.setNome(rs.getString("nome"));
            }

            rs.close();
            pst.close();
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,
                "Erro ao procurar dados! \n" + erro);
        }

        ligacao.desligaDb();

        return modelo;
    }
}
