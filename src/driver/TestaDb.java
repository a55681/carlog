/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/

package driver;

import java.sql.SQLException;

/**
 * The Class TestaDb.
 */
public class TestaDb {
  /**
   * The main method.
   *
   * @param args
   *          the arguments
   * @throws SQLException
   *           the SQL exception
   */
  public static void main(String[] args) throws SQLException {
    ConnDb liga = new ConnDb();
    liga.ligacaoDb();
  }
}
