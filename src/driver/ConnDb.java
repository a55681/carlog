/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/

package driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

// TODO: Auto-generated Javadoc
/**
 * The Class ConnDb.
 */
public class ConnDb {
  
  /** The stm. */
  public Statement  stm;

  /** The rs. */
  public ResultSet  rs;

  /** The caminho. */
  private String    caminho = "jdbc:sqlite:Carlog.db";

  /** The con. */
  public Connection con;

  /**
   * Ligacao db.
   */
  public void ligacaoDb() {
    try {
      con = DriverManager.getConnection(caminho);

      // JOptionPane.showMessageDialog(null, "Liga��o a base de dados com sucesso");
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Liga��o da base de dados com erro! \n" + erro);
    }
  }

  /**
   * Executarsql.
   *
   * @param sql
   *          the sql
   */
  public void executarsql(String sql) {
    try {
      stm = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
      rs = stm.executeQuery(sql);
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao executar sql! \n" + erro);
    }
  }

  /**
   * Desliga db.
   */
  public void desligaDb() {
    try {
      con.close();

      // JOptionPane.showMessageDialog(null, "Desligado da base de dados com sucesso");
    } catch (SQLException erro) {
      JOptionPane.showMessageDialog(null, "Erro ao desligar a base de dados! \n" + erro);
    }
  }
}
