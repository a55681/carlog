/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/

package model;

// TODO: Auto-generated Javadoc
/**
 * The Class MOficina.
 */
public class MOficina {

  /** The oficina id. */
  private int    oficinaId;

  /** The nome. */
  private String nome;

  /** The local. */
  private String local;

  /** The telefone. */
  private String telefone;

  /**
   * Gets the oficina id.
   *
   * @return the oficina id
   */
  public final int getOficinaId() {
    return oficinaId;
  }

  /**
   * Sets the oficina id.
   *
   * @param oficinaId
   *          the new oficina id
   */
  public final void setOficinaId(final int oficinaId) {
    this.oficinaId = oficinaId;
  }

  /**
   * Gets the nome.
   *
   * @return the nome
   */
  public final String getNome() {
    return nome;
  }

  /**
   * Sets the nome.
   *
   * @param nome
   *          the new nome
   */
  public final void setNome(final String nome) {
    this.nome = nome;
  }

  /**
   * Gets the local.
   *
   * @return the local
   */
  public final String getLocal() {
    return local;
  }

  /**
   * Sets the local.
   *
   * @param local
   *          the new local
   */
  public final void setLocal(final String local) {
    this.local = local;
  }

  /**
   * Gets the telefone.
   *
   * @return the telefone
   */
  public final String getTelefone() {
    return telefone;
  }

  /**
   * Sets the telefone.
   *
   * @param telefone
   *          the new telefone
   */
  public final void setTelefone(final String telefone) {
    this.telefone = telefone;
  }
}
