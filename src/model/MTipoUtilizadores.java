/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package model;

// TODO: Auto-generated Javadoc
/**
 * The Class MTipoUtilizadores.
 */
public class MTipoUtilizadores {

  /** The tipo utilizador id. */
  private int    tipoUtilizadorId;

  /** The descricao. */
  private String descricao;

  /** The pesquisa. */
  private String pesquisa;

  /**
   * Gets the pesquisa.
   *
   * @return the pesquisa
   */
  public final String getPesquisa() {
    return pesquisa;
  }

  /**
   * Sets the pesquisa.
   *
   * @param pesquisa
   *          the new pesquisa
   */
  public final void setPesquisa(final String pesquisa) {
    this.pesquisa = pesquisa;
  }

  /**
   * Gets the tipo utilizadores id.
   *
   * @return the tipo utilizadores id
   */
  public final int getTipoUtilizadoresId() {
    return tipoUtilizadorId;
  }

  /**
   * Sets the tipo utilizadores id.
   *
   * @param tipoUtilizadoresId
   *          the new tipo utilizadores id
   */
  public final void setTipoUtilizadoresId(final int tipoUtilizadoresId) {
    this.tipoUtilizadorId = tipoUtilizadoresId;
  }

  /**
   * Gets the descricao.
   *
   * @return the descricao
   */
  public final String getDescricao() {
    return descricao;
  }

  /**
   * Sets the descricao.
   *
   * @param descricao
   *          the new descricao
   */
  public final void setDescricao(final String descricao) {
    this.descricao = descricao;
  }
}
