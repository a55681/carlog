/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/

package model;

// TODO: Auto-generated Javadoc
/**
 * The Class MOperacao.
 */
public class MOperacao {

  /** The revisao id. */
  private int    revisaoId;

  /** The automovel id. */
  private int    automovelId;

  /** The valor. */
  private double valor;

  /** The quantidade. */
  private int    quantidade;

  /**
   * Gets the revisao id.
   *
   * @return the revisao id
   */
  public final int getRevisaoId() {
    return revisaoId;
  }

  /**
   * Sets the revisao id.
   *
   * @param revisaoId
   *          the new revisao id
   */
  public final void setRevisaoId(final int revisaoId) {
    this.revisaoId = revisaoId;
  }

  /**
   * Gets the automovel id.
   *
   * @return the automovel id
   */
  public final int getAutomovelId() {
    return automovelId;
  }

  /**
   * Sets the automovel id.
   *
   * @param automovelId
   *          the new automovel id
   */
  public final void setAutomovelId(final int automovelId) {
    this.automovelId = automovelId;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public final double getValor() {
    return valor;
  }

  /**
   * Sets the valor.
   *
   * @param valor
   *          the new valor
   */
  public final void setValor(final double valor) {
    this.valor = valor;
  }

  /**
   * Gets the quantidade.
   *
   * @return the quantidade
   */
  public final int getQuantidade() {
    return quantidade;
  }

  /**
   * Sets the quantidade.
   *
   * @param quantidade
   *          the new quantidade
   */
  public final void setQuantidade(final int quantidade) {
    this.quantidade = quantidade;
  }
}
