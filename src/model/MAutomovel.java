/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/

package model;

import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc

/**
 * The Class MAutomovel.
 */
public class MAutomovel {
  // Defini��o dos atributos

  /** The automovel id. */
  private int    automovelId;

  /** The marca. */
  private String marca;

  /** The modelo. */
  private String modelo;

  /** The cor. */
  private String cor;

  /** The cilindrada. */
  private int    cilindrada;

  /** The matricula. */
  private String matricula;

  /** The combustivel. */
  private String combustivel;

  /** The ano aquisicao. */
  private int    anoAquisicao;

  /** The ano venda. */
  private int    anoVenda;

  /** The preco aquisicao. */
  private float  precoAquisicao;

  /** The preco venda. */
  private float  precoVenda;

  /** The pesquisa. */
  private String pesquisa;

  /**
   * Gets the ano aquisicao.
   *
   * @return the ano aquisicao
   */
  public int getAnoAquisicao() {
    return anoAquisicao;
  }

  /**
   * Sets the ano aquisicao.
   *
   * @param anoAquisicao
   *          the new ano aquisicao
   */
  public void setAnoAquisicao(int anoAquisicao) {
    this.anoAquisicao = anoAquisicao;
  }

  /**
   * Sets the ano aquisicao.
   *
   * @param valueOf
   *          the new ano aquisicao
   */
  public void setAnoAquisicao(String valueOf) {
    int valor = 0;
    boolean isNumber = Pattern.matches("[0-9]+", valueOf);

    if (isNumber) {
      valor = Integer.parseInt(valueOf);
    }

    this.anoAquisicao = valor;
  }

  /**
   * Gets the ano venda.
   *
   * @return the ano venda
   */
  public int getAnoVenda() {
    return anoVenda;
  }

  /**
   * Sets the ano venda.
   *
   * @param anoVenda
   *          the new ano venda
   */
  public void setAnoVenda(int anoVenda) {
    this.anoVenda = anoVenda;
  }

  /**
   * Sets the ano venda.
   *
   * @param valueOf
   *          the new ano venda
   */
  public void setAnoVenda(String valueOf) {
    int valor = 0;
    boolean isNumber = Pattern.matches("[0-9]+", valueOf);

    if (isNumber) {
      valor = Integer.parseInt(valueOf);
    }

    this.anoVenda = valor;
  }

  /**
   * Gets the automovel id.
   *
   * @return the automovel id
   */
  public int getAutomovelId() {
    return automovelId;
  }

  /**
   * Sets the automovel id.
   *
   * @param automovelId
   *          the new automovel id
   */
  public void setAutomovelId(int automovelId) {
    this.automovelId = automovelId;
  }

  /**
   * Gets the cilindrada.
   *
   * @return the cilindrada
   */
  public int getCilindrada() {
    return cilindrada;
  }

  /**
   * Sets the cilindrada.
   *
   * @param valueOf
   *          the new cilindrada
   */
  public void setCilindrada(String valueOf) {
    int valor = 0;
    boolean isNumber = Pattern.matches("[0-9]+", valueOf);

    if (isNumber) {
      valor = Integer.parseInt(valueOf);
    }

    this.cilindrada = valor;
  }

  /**
   * Sets the cilindrada.
   *
   * @param cilindrada
   *          the new cilindrada
   */
  public void setCilindrada(int cilindrada) {
    this.cilindrada = cilindrada;
  }

  /**
   * Gets the combustivel.
   *
   * @return the combustivel
   */
  public String getCombustivel() {
    return combustivel;
  }

  /**
   * Sets the combustivel.
   *
   * @param combustivel
   *          the new combustivel
   */
  public void setCombustivel(String combustivel) {
    this.combustivel = combustivel;
  }

  /**
   * Gets the cor.
   *
   * @return the cor
   */
  public String getCor() {
    return cor;
  }

  /**
   * Sets the cor.
   *
   * @param cor
   *          the new cor
   */
  public void setCor(String cor) {
    this.cor = cor;
  }

  /**
   * Gets the marca.
   *
   * @return the marca
   */
  public String getMarca() {
    return marca;
  }

  /**
   * Sets the marca.
   *
   * @param marca
   *          the new marca
   */
  public void setMarca(String marca) {
    this.marca = marca;
  }

  /**
   * Gets the matricula.
   *
   * @return the matricula
   */
  public String getMatricula() {
    return matricula;
  }

  /**
   * Sets the matricula.
   *
   * @param matricula
   *          the new matricula
   */
  public void setMatricula(String matricula) {
    this.matricula = matricula;
  }

  /**
   * Gets the modelo.
   *
   * @return the modelo
   */
  public String getModelo() {
    return modelo;
  }

  /**
   * Sets the modelo.
   *
   * @param modelo
   *          the new modelo
   */
  public void setModelo(String modelo) {
    this.modelo = modelo;
  }

  /**
   * Gets the pesquisa.
   *
   * @return the pesquisa
   */
  public String getPesquisa() {
    return pesquisa;
  }

  /**
   * Sets the pesquisa.
   *
   * @param pesquisa
   *          the new pesquisa
   */
  public void setPesquisa(String pesquisa) {
    this.pesquisa = pesquisa;
  }

  /**
   * Gets the preco aquisicao.
   *
   * @return the preco aquisicao
   */

  /**
   * Gets the preco aquisicao.
   *
   * @return the preco aquisicao
   */
  public float getPrecoAquisicao() {
    return precoAquisicao;
  }

  /**
   * Sets the preco aquisicao.
   *
   * @param valueOf
   *          the new preco aquisicao
   */
  public void setPrecoAquisicao(String valueOf) {
    float valor = 0;
    this.precoAquisicao = valor;
  }

  /**
   * Sets the preco aquisicao.
   *
   * @param precoAquisicao
   *          the new preco aquisicao
   */
  public void setPrecoAquisicao(float precoAquisicao) {
    this.precoAquisicao = precoAquisicao;
  }

  /**
   * Sets the preco venda.
   *
   * @param valueOf
   *          the new preco venda
   */
  public void setPrecoVenda(String valueOf) {
    float valor = 0;
    this.precoVenda = valor;
  }

  /**
   * Sets the preco venda.
   *
   * @param precoVenda
   *          the new preco venda
   */
  public void setPrecoVenda(float precoVenda) {
    this.precoVenda = precoVenda;
  }

  /**
   * Gets the preco venda.
   *
   * @return the preco venda
   */
  public float getPrecoVenda() {
    return precoVenda;
  }
}
