/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package model;

// TODO: Auto-generated Javadoc
/**
 * The Class MTipoTaxa.
 */
public class MTipoTaxa {
  
  /** The nome. */
  private String nome;

  /** The pesquisa. */
  private String pesquisa;

  /**
   * Gets the pesquisa.
   *
   * @return the pesquisa
   */
  public final String getPesquisa() {
    return pesquisa;
  }

  /**
   * Sets the pesquisa.
   *
   * @param pesquisa
   *          the new pesquisa
   */
  public final void setPesquisa(final String pesquisa) {
    this.pesquisa = pesquisa;
  }

  /**
   * Gets the nome.
   *
   * @return the nome
   */
  public final String getNome() {
    return nome;
  }

  /**
   * Sets the nome.
   *
   * @param nome
   *          the new nome
   */
  public final void setNome(final String nome) {
    this.nome = nome;
  }
}
