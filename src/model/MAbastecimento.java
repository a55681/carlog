/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/

package model;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class MAbastecimento.
 */
public class MAbastecimento {
  
  /** The abastecimento id. */
  private int    abastecimentoId;

  /** The automovel id. */
  private int    automovelId;

  /** The data. */
  private Date   data;

  /** The local. */
  private String local;

  /** The kmsautomovel. */
  private float  kmsautomovel;

  /** The valor. */
  private float  valor;

  /** The litros. */
  private float  litros;

  /** The pesquisa. */
  private String pesquisa;

  /**
   * Gets the pesquisa.
   *
   * @return the pesquisa
   */
  public String getPesquisa() {
    return pesquisa;
  }

  /**
   * Sets the pesquisa.
   *
   * @param pesquisa
   *          the new pesquisa
   */
  public void setPesquisa(String pesquisa) {
    this.pesquisa = pesquisa;
  }

  /**
   * Gets the abastecimento id.
   *
   * @return the abastecimento id
   */
  public final int getAbastecimentoId() {
    return abastecimentoId;
  }

  /**
   * Sets the abastecimento id.
   *
   * @param abastecimentoId
   *          the new abastecimento id
   */
  public final void setAbastecimentoId(final int abastecimentoId) {
    this.abastecimentoId = abastecimentoId;
  }

  /**
   * Gets the automovel id.
   *
   * @return the automovel id
   */
  public final int getAutomovelId() {
    return automovelId;
  }

  /**
   * Sets the automovel id.
   *
   * @param automovelId
   *          the new automovel id
   */
  public final void setAutomovelId(final int automovelId) {
    this.automovelId = automovelId;
  }

  /**
   * Gets the data.
   *
   * @return the data
   */
  public Date getData() {
    return data;
  }

  /**
   * Sets the data.
   *
   * @param data
   *          the new data
   */
  public void setData(Date data) {
    this.data = data;
  }

  /**
   * Gets the local.
   *
   * @return the local
   */
  public final String getLocal() {
    return local;
  }

  /**
   * Sets the local.
   *
   * @param local
   *          the new local
   */
  public final void setLocal(final String local) {
    this.local = local;
  }

  /**
   * Gets the kmsautomovel.
   *
   * @return the kmsautomovel
   */
  public final float getKmsautomovel() {
    return kmsautomovel;
  }

  /**
   * Sets the kmsautomovel.
   *
   * @param kmsautomovel
   *          the new kmsautomovel
   */
  public final void setKmsautomovel(final float kmsautomovel) {
    this.kmsautomovel = kmsautomovel;
  }

  /**
   * Sets the kmsautomovel.
   *
   * @param valueOf
   *          the new kmsautomovel
   */
  public void setKmsautomovel(String valueOf) {
    float valor = 0;
    this.kmsautomovel = valor;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public final float getValor() {
    return valor;
  }

  /**
   * Sets the valor.
   *
   * @param valor
   *          the new valor
   */
  public final void setValor(final float valor) {
    this.valor = valor;
  }

  /**
   * Sets the valor.
   *
   * @param valueOf
   *          the new valor
   */
  public void setValor(String valueOf) {
    float valor = 0;
    this.valor = valor;
  }

  /**
   * Gets the litros.
   *
   * @return the litros
   */
  public final float getLitros() {
    return litros;
  }

  /**
   * Sets the litros.
   *
   * @param litros
   *          the new litros
   */
  public final void setLitros(final float litros) {
    this.litros = litros;
  }

  /**
   * Sets the litros.
   *
   * @param valueOf
   *          the new litros
   */
  public void setLitros(String valueOf) {
    float valor = 0;
    this.litros = valor;
  }
}
