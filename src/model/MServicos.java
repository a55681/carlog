/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package model;

// TODO: Auto-generated Javadoc
/**
 * The Class MServicos.
 */
public class MServicos {
  
  /** The toperacao id. */
  private int    toperacaoId;

  /** The nome. */
  private String nome;

  /** The preco. */
  private double preco;

  /**
   * Gets the toperacao id.
   *
   * @return the toperacao id
   */
  public final int getToperacaoId() {
    return toperacaoId;
  }

  /**
   * Sets the toperacao id.
   *
   * @param toperacaoId
   *          the new toperacao id
   */
  public final void setToperacaoId(final int toperacaoId) {
    this.toperacaoId = toperacaoId;
  }

  /**
   * Gets the nome.
   *
   * @return the nome
   */
  public final String getNome() {
    return nome;
  }

  /**
   * Sets the nome.
   *
   * @param nome
   *          the new nome
   */
  public final void setNome(final String nome) {
    this.nome = nome;
  }

  /**
   * Gets the preco.
   *
   * @return the preco
   */
  public final double getPreco() {
    return preco;
  }

  /**
   * Sets the preco.
   *
   * @param preco
   *          the new preco
   */
  public final void setPreco(final double preco) {
    this.preco = preco;
  }
}
