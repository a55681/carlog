/*
 * Copyright (c) 2016 by Pedro Rocha.
 *
 * 23/jun/2016
 *
 */

package model;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class MTaxaImposto.
 */
public class MTaxaImposto {

  /** The taxa imposto id. */
  private int    taxaImpostoId;

  /** The automovel id. */
  private int    automovelId;

  /** The data. */
  private Date   data;

  /** The valor. */
  private float  valor;

  /** The data validade. */
  private Date   dataValidade;

  /** The tipotaxa. */
  private String tipotaxa;

  /** The pesquisa. */
  private String pesquisa;

  /**
   * Gets the pesquisa.
   *
   * @return the pesquisa
   */
  public final String getPesquisa() {
    return pesquisa;
  }

  /**
   * Sets the pesquisa.
   *
   * @param pesquisa
   *          the new pesquisa
   */
  public final void setPesquisa(final String pesquisa) {
    this.pesquisa = pesquisa;
  }

  /**
   * Gets the taxa imposto id.
   *
   * @return the taxa imposto id
   */
  public final int getTaxaImpostoId() {
    return taxaImpostoId;
  }

  /**
   * Sets the taxa imposto id.
   *
   * @param taxaImpostoId
   *          the new taxa imposto id
   */
  public final void setTaxaImpostoId(final int taxaImpostoId) {
    this.taxaImpostoId = taxaImpostoId;
  }

  /**
   * Gets the automovel id.
   *
   * @return the automovel id
   */
  public final int getAutomovelId() {
    return automovelId;
  }

  /**
   * Sets the automovel id.
   *
   * @param automovelId
   *          the new automovel id
   */
  public final void setAutomovelId(final int automovelId) {
    this.automovelId = automovelId;
  }

  /**
   * Gets the data.
   *
   * @return the data
   */
  public final Date getData() {
    return data;
  }

  /**
   * Sets the data.
   *
   * @param data
   *          the new data
   */
  public final void setData(final Date data) {
    this.data = data;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public final float getValor() {
    return valor;
  }

  /**
   * Sets the valor.
   *
   * @param valor
   *          the new valor
   */
  public final void setValor(final float valor) {
    this.valor = valor;
  }

  /**
   * Sets the valor.
   *
   * @param valueOf
   *          the new valor
   */
  public void setValor(String valueOf) {
    float valor = 0;
    this.valor = valor;
  }

  /**
   * Gets the data validade.
   *
   * @return the data validade
   */
  public final Date getDataValidade() {
    return dataValidade;
  }

  /**
   * Sets the data validade.
   *
   * @param dataValidade
   *          the new data validade
   */
  public final void setDataValidade(final Date dataValidade) {
    this.dataValidade = dataValidade;
  }

  /**
   * Gets the tipotaxa.
   *
   * @return the tipotaxa
   */
  public final String getTipotaxa() {
    return tipotaxa;
  }

  /**
   * Sets the tipotaxa.
   *
   * @param tipotaxa
   *          the new tipotaxa
   */
  public final void setTipotaxa(final String tipotaxa) {
    this.tipotaxa = tipotaxa;
  }

}
