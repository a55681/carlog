/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package model;

// TODO: Auto-generated Javadoc
/**
 * The Class MTipoCombustivel.
 */
public class MTipoCombustivel {
  
  /** The combustivel. */
  private String combustivel;

  /** The pesquisa. */
  private String pesquisa;

  /**
   * Gets the pesquisa.
   *
   * @return the pesquisa
   */
  public final String getPesquisa() {
    return pesquisa;
  }

  /**
   * Sets the pesquisa.
   *
   * @param pesquisa
   *          the new pesquisa
   */
  public final void setPesquisa(final String pesquisa) {
    this.pesquisa = pesquisa;
  }

  /**
   * Gets the combustivel.
   *
   * @return the combustivel
   */
  public final String getCombustivel() {
    return combustivel;
  }

  /**
   * Sets the combustivel.
   *
   * @param combustivel
   *          the new combustivel
   */
  public final void setCombustivel(final String combustivel) {
    this.combustivel = combustivel;
  }
}
