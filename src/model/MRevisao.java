/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/

package model;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class MRevisao.
 */
public class MRevisao {

  /** The revisao id. */
  private int    revisaoId;

  /** The oficina. */
  private String oficina;

  /** The automovel id. */
  private int    automovelId;

  /** The kmsautomovel. */
  private float  kmsautomovel;

  /** The data. */
  private Date   data;

  /** The obeservacoes. */
  private String obeservacoes;

  /** The pesquisa. */
  private String pesquisa;

  /**
   * Gets the pesquisa.
   *
   * @return the pesquisa
   */
  public final String getPesquisa() {
    return pesquisa;
  }

  /**
   * Sets the pesquisa.
   *
   * @param pesquisa
   *          the new pesquisa
   */
  public final void setPesquisa(final String pesquisa) {
    this.pesquisa = pesquisa;
  }

  /**
   * Gets the revisao id.
   *
   * @return the revisao id
   */
  public final int getRevisaoId() {
    return revisaoId;
  }

  /**
   * Sets the revisao id.
   *
   * @param revisaoId
   *          the new revisao id
   */
  public final void setRevisaoId(final int revisaoId) {
    this.revisaoId = revisaoId;
  }

  /**
   * Gets the oficina.
   *
   * @return the oficina
   */
  public final String getOficina() {
    return oficina;
  }

  /**
   * Sets the oficina.
   *
   * @param oficina
   *          the new oficina
   */
  public final void setOficina(final String oficina) {
    this.oficina = oficina;
  }

  /**
   * Gets the automovel id.
   *
   * @return the automovel id
   */
  public final int getAutomovelId() {
    return automovelId;
  }

  /**
   * Sets the automovel id.
   *
   * @param automovelId
   *          the new automovel id
   */
  public final void setAutomovelId(final int automovelId) {
    this.automovelId = automovelId;
  }

  /**
   * Gets the kmsautomovel.
   *
   * @return the kmsautomovel
   */
  public final float getKmsautomovel() {
    return kmsautomovel;
  }

  /**
   * Sets the kmsautomovel.
   *
   * @param kmsautomovel
   *          the new kmsautomovel
   */
  public final void setKmsautomovel(final float kmsautomovel) {
    this.kmsautomovel = kmsautomovel;
  }

  /**
   * Sets the kmsautomovel.
   *
   * @param valueOf
   *          the new kmsautomovel
   */
  public final void setKmsautomovel(final String valueOf) {
    float valor = 0;
    this.kmsautomovel = valor;
  }

  /**
   * Gets the data.
   *
   * @return the data
   */
  public final Date getData() {
    return data;
  }

  /**
   * Sets the data.
   *
   * @param data
   *          the new data
   */
  public final void setData(final Date data) {
    this.data = data;
  }

  /**
   * Gets the obeservacoes.
   *
   * @return the obeservacoes
   */
  public final String getObeservacoes() {
    return obeservacoes;
  }

  /**
   * Sets the obeservacoes.
   *
   * @param obeservacoes
   *          the new obeservacoes
   */
  public final void setObeservacoes(final String obeservacoes) {
    this.obeservacoes = obeservacoes;
  }
}
