/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package model;

// TODO: Auto-generated Javadoc
/**
 * The Class MUtilizadores.
 */
public class MUtilizadores {
  // Atributos
  /** The utilizador id. */
  private int    utilizadorId;

  /** The nome. */
  private String nome;

  /** The password. */
  private String password;

  /** The tipo. */
  private int    tipo;

  /** The pesquisa. */
  private String pesquisa;

  /**
   * Gets the pesquisa.
   *
   * @return the pesquisa
   */
  public final String getPesquisa() {
    return pesquisa;
  }

  /**
   * Sets the pesquisa.
   *
   * @param pesquisa
   *          the new pesquisa
   */
  public final void setPesquisa(final String pesquisa) {
    this.pesquisa = pesquisa;
  }

  /**
   * Gets the utilizador id.
   *
   * @return the utilizador id
   */
  public final int getUtilizadorId() {
    return utilizadorId;
  }

  /**
   * Sets the utilizador id.
   *
   * @param utilizadorId
   *          the new utilizador id
   */
  public final void setUtilizadorId(final int utilizadorId) {
    this.utilizadorId = utilizadorId;
  }

  /**
   * Gets the nome.
   *
   * @return the nome
   */
  public final String getNome() {
    return nome;
  }

  /**
   * Sets the nome.
   *
   * @param nome
   *          the new nome
   */
  public final void setNome(final String nome) {
    this.nome = nome;
  }

  /**
   * Gets the password.
   *
   * @return the password
   */
  public final String getPassword() {
    return password;
  }

  /**
   * Sets the password.
   *
   * @param password
   *          the new password
   */
  public final void setPassword(final String password) {
    this.password = password;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public final int getTipo() {
    return tipo;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo
   *          the new tipo
   */
  public final void setTipo(final int tipo) {
    this.tipo = tipo;
  }
}
