/*
* Copyright (c) 2016 by Pedro Rocha.
*
* 23/jun/2016
*
*/
package model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

// TODO: Auto-generated Javadoc
/**
 * The Class MTabela.
 */
public class MTabela extends AbstractTableModel {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The linhas. */
  private ArrayList<?>      linhas           = null;

  /** The colunas. */
  private String[]          colunas          = null;

  /**
   * Instantiates a new m tabela.
   *
   * @param lin
   *          the lin
   * @param col
   *          the col
   */
  public MTabela(final ArrayList<?> lin, final String[] col) {
    // TODO Auto-generated constructor stub
    setLinhas(lin);
    setColunas(col);
  }

  /**
   * Gets the linhas.
   *
   * @return the linhas
   */
  public final ArrayList<?> getLinhas() {
    return linhas;
  }

  /**
   * Sets the linhas.
   *
   * @param linhas
   *          the new linhas
   */
  public final void setLinhas(final ArrayList<?> linhas) {
    this.linhas = linhas;
  }

  /**
   * Gets the colunas.
   *
   * @return the colunas
   */
  public final String[] getColunas() {
    return colunas;
  }

  /**
   * Sets the colunas.
   *
   * @param colunas
   *          the new colunas
   */
  public final void setColunas(final String[] colunas) {
    this.colunas = colunas;
  }

  /*
   * (non-Javadoc)
   *
   * @see javax.swing.table.TableModel#getColumnCount()
   */
  @Override
  public final int getColumnCount() {
    // TODO Auto-generated method stub
    return colunas.length;
  }

  /*
   * (non-Javadoc)
   *
   * @see javax.swing.table.TableModel#getRowCount()
   */
  @Override
  public final int getRowCount() {
    // TODO Auto-generated method stub
    return linhas.size();
  }

  /*
   * (non-Javadoc)
   *
   * @see javax.swing.table.AbstractTableModel#getColumnName(int)
   */
  @Override
  public final String getColumnName(final int numCol) {
    return colunas[numCol];
  }

  /*
   * (non-Javadoc)
   *
   * @see javax.swing.table.TableModel#getValueAt(int, int)
   */
  @Override
  public final Object getValueAt(final int numLin, final int numCol) {
    Object[] linha = (Object[]) getLinhas().get(numLin);

    return linha[numCol];
  }
}
